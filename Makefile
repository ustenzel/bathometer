LDLIBS += -lz -lpthread
CFLAGS += -Wall -Ofast -ggdb -march=native -mtune=native

all: bathometer 

bathometer.1: bathometer.1.md
	pandoc -o $@ $< -f markdown -t man

bathometer.html: bathometer.1.md
	pandoc -o $@ $< -f markdown -t html

# to typeset the appnote, texlive, latexmk, texlive-fonts-extra needs to be installed
appnote/appnote_final.pdf: appnote/appnote_final.tex appnote/appnote.tex appnote/appnote.bib
	cd appnote ; latexmk -pdf appnote_final

appnote/appnote_draft.pdf: appnote/appnote_draft.tex appnote/appnote.tex appnote/appnote.bib
	cd appnote ; latexmk -pdf appnote_draft

clean:
	-rm bathometer bathometer.o indexer.o msqueue.o pindexer.o query.o
	-rm tests/tiny.bam tests/tiny.dx tests/tiny.bam.bai

bathometer: bathometer.o indexer.o msqueue.o pindexer.o query.o

bathometer.o: bathometer.h
blobtest.o:   bathometer.h
indexer.o:    bathometer.h
msqueue.c:    msqueue.h
pindexer.o:   bathometer.h msqueue.h
query.o:      bathometer.h select.h
