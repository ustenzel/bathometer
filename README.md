# Bathometer #

This is a method to index BAM files in such a way that the read depth in
any region can be queried efficiently.  It works even with unsorted
files, if only because it's possible, and because you never really know
if output from `samtools` is actually sorted.

## Setup ##

You need a not too ancient C compiler and Zlib.  On Debian and
derivatives, these amount to installing `build-essentials` and
`zlib1g-dev`.  

## Quickstart ##

See `tests/tiny.sh` and `tests/tiny.sam` for an example workflow.
Also see `tests/megatest.sh` and `tests/wgstest.sh` for the evalutations
we did for the Application Note, and see `tests/qtest.pl` for a
universal stress test.

Run `bathometer index <indexfile> <input.bam>` to create an index for a
BAM file.  Only one BAM file per sample can be read, because of the
complications arising when the reference sequences of multiple files
don't match.  (If you store one BAM file per chromosome or somesuch, you
are confused.  Multiple files can always be concatenated or merged or
melded by other readily available tools.)

Run `bathometer query <index1> <index2> ... ::: <query1>
<query2> ...` to query the indices.  Multiple indices and/or queries are
supported, queries can be BED, GFF or `chr:from-end:strand` directly on
the command line.  Use `bathometer fpkm` instead of `bathometer query`
to get FPKM instead of raw counts.  Every input line is repeated on
stdout, and if it defines a region, one new tab-separated field with the
result of the query is appended for each index.  (This is not a standard
file format, but it should be possible to `awk` the output to taste.
Use less bizarre inputs to get less bizarre outputs.)


## How can it be so fast? ##

We store read coordinates in a compact data structure as follows. 
Let &Phi;(i) be the i-th position at which a read starts, let
&Psi;(j) be the j-th position at which a read ends.  (We use zero-based
indexing and half-open intervals throughout.)  Both are monotonic
sequences and can be stored compactly using the Elias-Fano encoding.
For a nice, accessible explanation see for example
[Antonio Mallia's blog](https://www.antoniomallia.it/sorted-integers-compression-with-elias-fano-encoding.html).

The number of reads starting before position p is then &Phi;<sup>-1</sup>(p), the
number of reads ending before q is &Psi;<sup>-1</sup>(q).  (This takes some care
in handling the edge cases right.)  We can compute the number of reads
overlapping the interval \[p,q) as &Phi;<sup>-1</sup>(q) - &Psi;<sup>-1</sup>(p+1).

Computing &Phi;<sup>-1</sup> requries a `select` operation on the "upper" part of
the code.  We can use Sebastiano Vigna's "simple" method to implement it
with low overhead.  A
[presentation by Vigna](https://shonan.nii.ac.jp/archives/seminar/029/wp-content/uploads/sites/12/2013/07/Sebastiano_Shonan.pdf)
that explains the method is available online.

To answer a query, the index is mapped into memory.  Accessing the &Phi;
and &Psi; arrays typically requires only three pages each to be read from
disk.  If the index resides on a solid state drive, the answer arrives
nearly instantaneously.  Even on spinning rust, we're looking at only
100ms or so.  


## About that memory consumption ##

Since the index is constructed in memory, parallel indexing can be
limited by available RAM.  Estimating the memory consumption may come in
handy.  Let M be the length of the genome and N the number of reads to
be indexed.  Define

L := floor( log2( 2M/N ) )

Then the index occupies approximately

N * (L/4 + 17/32) 

bytes.  It's not quite exact, due to padding and rounding.  Twice that
amount plus an overhead of a few megabytes is needed to construct the
index, to allow for reallocation and copying of the internal data
structures.

Consider a typical input from RNAseq.  M is 3 billion, N is 80 million.
Then L works out to 8, and the index size is 

80M * (2+17/32) &approx; 200MiB 

Therefore, `bathometer` should run fine in 400MiB of RAM, and 20
instances could be run on a common machine with 8GiB of RAM.

The actual index size was 173MiB, for a file of 4.75GiB.  As a rule of
thumb, "5% of the original file size" should work fine.



