## TODO ##

- check how big the inventory needs to be; see if 64 bit integers are needed
- can the last blob be built directly to file?
- does the blob need two header words?  (maxpos may well be redundant)
- can we store L somewhere convenient?

### nice to have
  
- support filter flags -m, -f, -F
  - store them, have `summary` report them
- long form query
  - query depth at every position within a region
  - requires linear scan of the E-F-code: sexy new algorithm
- conserve memory
  - process sorted bam files differently 
  - easy for start coordinate, intricate for end coordinate
- support other file formats
  - anything specifying ranges would work:  SAM, BED, GFF.
- beautify manpage
- pipe (or tee) mode for indexing
- support N operation in BAM
  - assuming the N codes for splicing, should it simply split a read?
