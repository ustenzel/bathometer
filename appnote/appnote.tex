\copyrightyear{2021} \pubyear{2021}

\access{Advance Access Publication Date: Day Month Year}
\appnotes{Applications Note}
\raggedbottom

\DeclareMathOperator{\select}{select}

\begin{document}
\firstpage{1}

\subtitle{Sequence analysis}

\title[bathometer]{bathometer: lightning fast depth-of-reads query}

\author[Stenzel and Horn]{U.~Stenzel\,$^{\text{\sfb 1,}*}$ and
S.~Horn\,$^{\text{\sfb 1,}}$\,$^{\text{\sfb 2,}}$\,$^{\text{\sfb 3}}$}
\address{$^{\text{\sf 1}}${Rudolf Schönheimer Institute of Biochemistry,
University of Leipzig, Johannisallee 30, 04103 Leipzig, Germany.}\\
$^{\text{\sf 2}}${Department of Dermatology, University Hospital Essen,
Hufelandstr. 55, 45122 Essen, Germany.}\\
$^{\text{\sf 3}}${German Cancer Consortium (DKTK), University Hospital
Essen, Hufelandstr. 55, 45122 Essen, Germany.}}

\corresp{$^\ast$To whom correspondence should be addressed.}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\abstract{\textbf{Motivation:} The query for the number of reads
overlapping a given region is a common step in the analysis of Illumina
sequencing data.  Sometimes, these queries are not conveniently
precomputable.  It seems beneficial to make this kind of arbitrary query
as fast and convenient as possible. \\
%
\textbf{Results:} We present Bathometer, a tool that indexes BAM files in a space
efficient way, which allows ad hoc queries for the number of reads
overlapping any given genomic region to be answered much more quickly
than by counting with common tools such as Samtools, while
incurring much less disk I/O. \\
%
\textbf{Availability:} Bathometer is implemented in C, licensed under the GNU General Public
License version~3+, and freely downloadable from
Bitbucket\footnote{\added{
    \href{https://bitbucket.org/ustenzel/bathometer}{https://bitbucket.org/ustenzel/bathometer}}} \\
\\
%
\textbf{Contact:} \href{u.stenzel@web.de}{u.stenzel@web.de} \\
\textbf{Supplementary data:} 
\href{https://bitbucket.org/ustenzel/bathometer/downloads/example\_query.bed}%
{https://bitbucket.org/ustenzel/bathometer/downloads/example\_query.bed} \\
}

\maketitle

\section{Features}

To use Bathometer, one or more BAM files have to be indexed by running
\texttt{bathometer index}.  The index, which has a size of approximately 5\% 
that of the input file, is by itself sufficient to answer queries.  The
input does not need to be sorted, and, at least in principle, does not
even need to be in BAM format.  This is in contrast to
Samtools[\citealp{Li:2009:Bioinformatics:19505943}], which
needs both an index and the original, sorted BAM file.

Queries are run against one or more indices using \texttt{bathometer query} or
\texttt{bathometer fpkm}.  The former counts reads overlapping query regions,
while the latter additionally normalizes these counts and reports FPKM
(Fragments Per Kilobase and Million reads mapped), which is customary in
the analysis of RNA sequencing experiments.  Queries can be in the common file
formats BED or GFF, or can be specified on the command line.  In
either case, Bathometer repeats the queries to its standard output,
and adds the results in additional columns, thus directly
producing a matrix with one sample per column and one query per row.

It should be noted that Bathometer effectively counts reads when
creating the index.  Consequently, any filters have to be applied at the
index creation stage, and further filtering at the query stage cannot be
supported.


\section{Data Structures}

Bathometer aims for an index that is compact and can be used without
having to be read into memory completely.  An index stores for
each strand of each reference sequence the list of starting positions
and the list of end positions of all reads.

The lists of positions are both increasing lists of integers, and are
stored in Elias-Fano coding[\citealp{elias1972}].  Briefly, this coding
splits each position into a low part and a high part, where the low part
is stored explicitly in binary, forming a vector $L$, while for the high
part, differences between consecutive entries are stored in unary,
forming the bitvector $H$.  The split is chosen in such a way that
approximately half the bits in $H$ are set.

To compute the number of reads overlapping interval $(a,b)$,
Bathometer subtracts the number of reads ending before $a$ from the
number of reads starting before $b$.  This requires it to find the
length of the shortest prefix of $H$ that contains a given number $n$ of
set bits (usually denoted $\select_1(H,n)$).  This is supported in
practically constant time by the "simple" select method of
[\citealp{vigna2008}]: $H$ is augmented with an inventory table that
contains the prefix lengths for round numbers.  $\select$ is realized by
a lookup in the inventory, followed by scanning a very limited stretch
of $H$ from the indicated position.

The Elias-Fano coding has a size very close to the information-theoric
limit.  Thus, the Bathometer index is very nearly the smallest index
that can conceivably support range queries.  Furthermore, queries need
to access a miniscule portion of the index:  two entries of each inventory,
a few words of each $H$ vector, and one entry of each $L$ vector. 
With the index already in RAM, this
incurs only six cache misses, and with the index residing on secondary
storage, the operating system will typically have to read six pages of
4kB each into RAM.  On today's common computers, a substantial number of
indices could realistically remain in RAM concurrently.  Additionally,
Bathometer has to parse some metainformation once per query, but this
overhead is negligible for batch queries.

In contrast, any program that aims to answer similar queries using the
standard BAM index, will have to parse the whole index, and the header
of the BAM file at least once per invocation.  Then for every query,
multiple "bins" have to be looked up in the index structure, and pieces of
the BAM file have to be loaded, decompressed and parsed for each
non-empty bin.  This necessarily involves both more disk I/O and more
processing, and it requires more space (either RAM or disk) to keep both
the index and the raw data available.


\section{Performance Comparison}

We compared the performance of \texttt{bathometer~query} to running
\texttt{samtools~view~-c~-L}, which produces useless output, but executes
approximately the equivalent query.  Table \ref{Tab:01} lists the
time and memory requirements to build the index \added{(the needed disk space
consists of both the index and the raw data for Samtools, but only the
index for Bathometer)}, table \ref{Tab:02}
lists the times to run the queries.  The test data result from RNA
sequencing of melanoma tumors of 91 patients[\cite{gide2019}], available
under the accession PRJEB23709 at ENA \added{(the \emph{Gide} dataset) and
approximately 8GB of whole genome shotgun sequences of wheat, available
under the accessions SRR12687020 and SRR12687022 at ERA (the
\emph{Wheat} dataset)}.  Sequencing reads were mapped to
the human reference GRCh38 \added{or the Wheat reference genome 1.30} using
Hisat2.  The query is \added{either} a BED file
containing 5818~regions, see supplementary data, or \added{the genome
annotation for wheat as obtained from NCBI}.

All tests were run on an Intel(R) Xeon(R) Gold~6244 with 384MiB of RAM
under Linux.  The data resided on a distributed Ceph filesystem backed
by hard drives ("disk"), or on a local SSD ("ssd"), or in the buffer
cache ("cache") after running the same query twice.  Lacking a
sufficiently large SSD for the full dataset, we ran the Samtools
\added{\emph{Gide}} tests
on the first seven patients and extrapolated to the full dataset. \vspace*{12pt}

\DEL{\begin{table}[!t]
        \processtable{\del{Resources needed to index the \emph{Gide} dataset}}
    {\del{\begin{tabular}{@{}llll@{}}\toprule & CPU time & Wallclock time & Memory\\\midrule
        Bathometer & 4h48 & 1h48 & 4.65GiB \\
        Samtools   & 1h40 & 1h48 & 0.25GiB \\\botrule
    \end{tabular}}}{}
\end{table}}

\begin{table}[!t]
    \processtable{\ins{Resources needed to index two datasets}\label{Tab:01}}
    {\ins{\begin{tabular}{@{}llllll@{}}\toprule & User & System & Wallclock & Memory & Disk\\\midrule
        Bathometer (Gide)  & 4h17m & 3m & 4h40m & 1.36GiB & 21.2GiB  \\
        Samtools   (Gide)  & 1h33m & 3m & 2h37m & 0.24GiB & 701GiB   \\\midrule
        Bathometer (Wheat) & 2m16s & 1.9s & 3m5s  & 516MiB & 0.37GiB \\
        Samtools   (Wheat) & 44s   & 1.2s & 49s   &  80MiB & 5.73GiB \\\botrule
    \end{tabular}}}{}
\end{table}

\DEL{ \begin{table}[!t]
    \processtable{\del{Time to query the \emph{Gide} dataset for 5818 regions}}
    {\del{\begin{tabular}{@{}llll@{}}\toprule & disk & ssd & cache\\\midrule
        Bathometer &       311s &   7.5s &   1.4s \\
        Samtools   &      6330s &  6460s &  6360s \\\botrule
    \end{tabular}}}{}
\end{table}}

\begin{table}[h]
    \processtable{\ins{Time to query two datasets}\label{Tab:02}}
    {\ins{\begin{tabular}{@{}llll@{}}\toprule & User & System & Wallclock \\\midrule
        Bathometer \emph{(Gide)}, disk  & 1s & 4.5s & 3m \\
        Bathometer \emph{(Gide)}, cache & 1.7s & 8.1s & 1m3s \\
        Bathometer \emph{(Gide)}, ssd   & 0.5s & 0.7s & 1.2s \\\midrule
%
        Samtools \emph{(Gide)}, disk  & 1h58m & 2m & 3h23m \\
        Samtools \emph{(Gide)}, cache & 1h59m & 2m & 2h17m \\
        Samtools \emph{(Gide)}, ssd   & 11m  & 23s & 12m   \\\midrule
%
        Bathometer \emph{(Wheat)}, disk  & 0.23s & 0.03s & 0.54s \\
        Bathometer \emph{(Wheat)}, cache & 0.28s & 0.02s & 0.20s \\
        Bathometer \emph{(Wheat)}, ssd   & 0.17s & 0.03s & 0.21s \\\midrule
%
        Samtools \emph{(Wheat)}, disk  & 50s & 1.5s & 1m6s \\
        Samtools \emph{(Wheat)}, cache & 46s & 1.0s & 47s \\
        Samtools \emph{(Wheat)}, ssd   & 46s & 1.4s & 48s   \\\botrule
    \end{tabular}}}{}
\end{table}


\section*{Funding}

This work was partly funded by the Deutsche Forschungsgemeinschaft (DFG,
German Research Foundation; HO~6389/2-1 [KFO~337], 6389/3-1
[Cryptamel]). 

\bibliography{appnote}{}
\bibliographystyle{natbib}

\end{document}
