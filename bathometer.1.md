= SYNOPSIS

  bathometer index [-@_num_] _output.idx_ [_input.bam_]
  bathometer query [_index.idx_...] ::: [_region_|_regions.bed_|_regions.gff_]
  bathometer fpkm [_index.idx_...] ::: [_region_|_regions.bed_|_regions.gff_]
  bathometer summary _index.idx_

= DESCRIPTION

`bathometer` indexes one or more bam files in a way that allows very
quick counting of the number of reads overlapping any given region.  

To build an index, run `bathometer index` on one or more BAM files.
Call `bathometer query` to count reads overlapping regions, or
`bathometer fpkm` to count reads and normalize for the total number of
mapped reads.  A quick summary is available with `bathometer summary`.


= COMMANDS

== index

bathometer index [-@_num_] _index.idx_ _input.bam_

Reads one BAM file and creates an index for it.  If no file name is
given, stdin is read instead.  The file name "-" also refers to stdin.

It is acceptable to index only a subset of reads, for example by running
`samtools view` with appropriate filters and piping the result into
`bathometer`.

If `-@_num_` is specified, Bathometer will try to run `num` threads to
speed up decompression of bam and construction of the index.

== query / fpkm

bathometer query [_index.idx_...] ::: [_region_|_regions.bed_|_regions.gff_]
bathometer fpkm [_index.idx_...] ::: [_region_|_regions.bed_|_regions.gff_]

Queries one or more indices for zero or more regions.  Both functions
work exactly the same, except that `query` counts reads in regions and
`fpkm` normalizes the counts for the total number of mapped reads.

The arguments should be one or more index files, followed by the
separator `:::`, followed by zero or more queries.  If no separator is
present, the first argument specifies the index, all further arguments
specify queries.

Queries can be regions of the form `chr:start-end[:strand]`, bed files,
or gff files.  These formats can even be mixed, however, the mixed
output will be rather hard to deal with.  If no queries are given, stdin
is read.

The output starts with a header line listing the index file names.
Following it, all queries are concatenated and repeated one per line,
with one additional column for each index, separated by tab characters.
While this is no standard format, it should be reasonably easy to parse
with standard tools.

== summary

bathometer summary _index.idx_

For each target sequence in an index, reports the total number of reads
mapped there.  An additional line reports the total number of reads
mapped anywhere.

= AUTHORS

Bathometer is Copyright (C) 2021 by Udo Stenzel <u.stenzel@web.de>.

