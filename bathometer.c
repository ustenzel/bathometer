/*
    This file is part of Bathometer.

    Bathometer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bathometer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Bathometer.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bathometer.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <zlib.h>

int usage()
{
    fprintf( stderr,
            "Bathometer, version 1.1\n"
            "Copyright (C) 2021 Udo Stenzel\n"
            "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"
            "\n"
            "This is free software; you are free to change and redistribute it.\n"
            "There is NO WARRANTY, to the extent permitted by law.\n"
            "\n"
            "Usage: bathometer index [-@num] output.idx [input.bam]\n"
            "       bathometer query [index.idx...] ::: [region|regions.bed|regions.gff]\n"
            "       bathometer fpkm [index.idx...] ::: [region|regions.bed|regions.gff]\n"
            "       bathometer summary index.idx\n"
            ) ;
    return 1 ;
}

char *argv0 = 0 ;

void croak(int status, int errnum, const char *format, ...)
{
    fflush( stdout ) ;
    fputs( argv0, stderr ) ;
    putc( ':', stderr ) ;
    putc( ' ', stderr ) ;

    va_list ap ;
    va_start( ap, format ) ;
    vfprintf( stderr, format, ap ) ;
    va_end( ap ) ;

    if( errnum ) {
        putc( ':', stderr ) ;
        putc( ' ', stderr ) ;
        fputs( strerror( errnum ), stderr ) ;
    }
    putc( '\n', stderr ) ;
    fflush( stderr ) ;
    if( status ) exit( status ) ;
}


// Determines if the file f has an index.  (If a bam file has an index,
// we assume it is sorted, no matter what the header says.  This is
// necessary, because too many versions of samtools are in circulation,
// which forget to declare the sort order.)
int has_index( char *f )
{
    int l = strlen(f) ;
    char* s = alloca( l + 5 ) ;
    strcpy( s, f ) ;
    strcpy( s+l, ".bai" ) ;
    if( 0 == access( s, F_OK ) ) return 1 ;

    strcpy( s+l, ".csi" ) ;
    return 0 == access( s, F_OK ) ;
}

int main_index( int argc, char **argv )
{
    int fd = 0 ;
    int is_sorted = 0 ;
    int nthreads = 0 ;
    if( argc > 0 && argv[0][0] == '-' && argv[0][1] == '@' ) {
        if( argv[0][2] ) {
            nthreads = atoi( argv[0]+2 ) ;
        } else if( argc > 1 ) {
            nthreads = atoi( argv[1] ) ;
            argc-- ;
            argv++ ;
        } else return usage() ;
        argc-- ;
        argv++ ;
    }

    if( argc == 2 ) {
        if( strcmp( argv[1], "-" ) ) {
            fd = open( argv[1], O_RDONLY ) ;
            is_sorted = has_index( argv[1] ) ;
        }
    }
    else if( argc != 1 ) return usage() ;

    struct index_builder seqs = nthreads > 0 ?
           pReadBam( fd, (argc == 1 ? "stdin" : argv[1]), is_sorted, (nthreads+1)/2 ) :
           readBam(  fd, (argc == 1 ? "stdin" : argv[1]), is_sorted ) ;
    close( fd ) ;

    build_index_file( argv[0], &seqs ) ;
    free_index_builder( &seqs ) ;
    return 0 ;
}

int main_summary( int argc, char **argv )
{
    int nommap = 0 ;
    if( argc && !strcmp( *argv, "--nommap" ) ) {
        nommap = 1 ;
        argc-- ;
        argv++ ;
    }
    if( argc != 1 ) return usage() ;

    index_t ix = open_index_file( argv[0], nommap ) ;
    blob phi = get_phi( &ix ) ;
    blob psi = get_psi( &ix ) ;

    // sanity checks
    assert( paramL(phi) == paramL(psi) ) ;
    assert( maxpos(phi) < maxpos(psi) ) ;
    assert( npos(phi) == npos(psi) ) ;
    assert( npos(phi) == eval( phi, 2*get_genome_len( &ix ) ) ) ;
    assert( npos(psi) == eval( phi, 2*get_genome_len( &ix ) ) ) ;

    printf( "rname\tnumreads+\tnumreads-\n" ) ;
    char *nm = (char*)( ix.base + 4 + 2 * get_nref( &ix ) ) + 4 ;
    for( int i = 0 ; i != get_nref( &ix ) ; ++i )
    {
        printf( "%s", nm ) ;
        for( int str = 0 ; str != 2 ; str++ )
        {
            uint64_t gg = (uint64_t)str * get_genome_len( &ix ) ;
            uint64_t count_phi = eval( phi, get_end_offset( &ix, i ) + gg ) - eval( phi, get_start_offset( &ix, i ) + gg ) ;
            uint64_t count_psi = eval( psi, get_end_offset( &ix, i ) + gg ) - eval( psi, get_start_offset( &ix, i ) + gg ) ;

            if( count_phi == count_psi )
            {
                // This is as expected, so print minimal information
                printf( "\t%"PRIu64, count_phi ) ;
            }
            else
            {
                // This is fucked up and needs debugging
                // (We should never get here.)
                croak( 2, 0, "internal inconsistency for refseq %d:\n"
                             "count(phi[%d])=%"PRIu64", count(psi[%d])=%"PRIu64", "
                             "L(phi)=%"PRIu64", L(psi)=%"PRIu64".\n"
                             "Please file a bug report.",
                       i, str, count_phi, str, count_psi, paramL(phi), paramL(psi) ) ;
            }
        }
        putchar('\n') ;
        nm += (strlen(nm) + 8) & ~3 ;
    }
    close_index_file( &ix ) ;
    return 0 ;
}

// If one of the arguments is ":::", then we have multiple index files.  Else the first argument is the only index file.
int main_query( int argc, char **argv, int want_fpkm )
{
    int indexc = 0 ;
    int nommap = 0 ;
    if( argc && !strcmp( *argv, "--nommap" ) ) {
        nommap = 1 ;
        argc-- ;
        argv++ ;
    }

    char **indexv = argv ;
    while( indexc != argc && strcmp( argv[indexc], ":::" ) ) ++indexc ;
    if( indexc == argc ) {
        indexc = 1 ;
        argc -= 1 ;
        argv += 1 ;
    } else {
        argc -= indexc + 1 ;
        argv += indexc + 1 ;
    }
    if( indexc < 1 ) return usage() ;

    int hdrlen = 0 ;
    index_t *ix = malloc( indexc * sizeof(index_t) ) ;
    for( int i = 0 ; i != indexc ; ++i )
    {
        ix[i] = open_index_file( indexv[i], nommap ) ;
        hdrlen += strlen( indexv[i] ) + 1 ;
    }

    char *hdr = 0 ;
    if( indexc > 1 ) {
        hdr = malloc( hdrlen ) ;
        char *phdr = hdr ;
        for( int i = 0 ; i != indexc ; ++i )
        {
            char *p = indexv[i] + strlen( indexv[i] ) ;
            while( p != indexv[i] && p[-1] != '/' && p[-1] != '.' ) --p ;
            if( p[-1] == '.' ) *--p = 0 ;
            while( p != indexv[i] && p[-1] != '/' ) --p ;
            *phdr++ = '\t' ;
            phdr = stpcpy( phdr, p ) ;
        }
    }

    if( argc == 0 ) query_fd( ix, ix+indexc, 0, hdr, want_fpkm ) ;
    else for( int argi = 0 ; argi != argc ; ++argi )
    {
        if( strcmp( argv[argi], "-" ) == 0 ) query_fd( ix, ix+indexc, 0, hdr, want_fpkm ) ;
        else if( strchr( argv[argi], '/' ) ) query_file( ix, ix+indexc, argv[argi], hdr, want_fpkm ) ;
        else if( strchr( argv[argi], ':' ) ) query_line( ix, ix+indexc, argv[argi], hdr, want_fpkm ) ;
        else query_file( ix, ix+indexc, argv[argi], hdr, want_fpkm ) ;
        free( hdr ) ;
        hdr = 0 ;
    }
    free( hdr ) ;

    for( int i = 0 ; i != indexc ; ++i ) close_index_file( ix+i ) ;
    free( ix ) ;
    return 0 ;
}

int main( int argc, char **argv )
{
    argv0 = argv[0] ;

    if( argc < 2 ) return usage() ;
    if( strcmp( argv[1],   "index" ) == 0 ) return   main_index( argc-2, argv+2 ) ;
    if( strcmp( argv[1],   "query" ) == 0 ) return   main_query( argc-2, argv+2, 0 ) ;
    if( strcmp( argv[1],    "fpkm" ) == 0 ) return   main_query( argc-2, argv+2, 1 ) ;
    if( strcmp( argv[1], "summary" ) == 0 ) return main_summary( argc-2, argv+2 ) ;
    return usage() ;
}


