/*
    This file is part of Bathometer.

    Bathometer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bathometer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Bathometer.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INCLUDED_BATHOMETER_H
#define INCLUDED_BATHOMETER_H

#include <stddef.h>
#include <stdint.h>
#include <unistd.h>
#include <x86intrin.h>

// As far as the user is concerned, blobs are opaque arrays of QWORDs.
//
// Internally, it consists of:
// - two header QWORDs
// - the vector L, padded to a full QWORD
// - the vector H, padded to a full QWORD
// - the inventory I, padded to a full QWORD.
//
// Vector L stores l lower bits of every position.  Vector H stores a
// one bit for every increment of the upper bits of the position, and a
// zero bit for every stored position.  The inventory is an array of 32
// bit integers such that I[x-1] = select_1( H, 256*x ).  More precisely,
// the first I[x-1] bits of H contain exactly 256*x one bits, and
// H[ I[x]-1 ] is a one bit.
typedef uint64_t *blob ;
typedef uint64_t const *const_blob ;

// Returns the number of stored positions.  We keep this in the first qword.
static inline uint64_t npos( const_blob b ) { return b ? b[0] : 0 ; }

// Returns the largest position stored.  We keep this in the second qword.
static inline uint64_t maxpos( const_blob b ) { return b ? b[1] : 0 ; }

// Determines the parameter L, that is the number of bits stored in the L vector.
static inline uint64_t paramL( const_blob b )
{
    if( b ) {
        uint32_t r = maxpos(b) / npos(b) ;
        return r > 1 ? 32 - __builtin_clz( r-1 ) : 0 ;
    }
    else return 0 ;
}

// Returns the number of positions stored in the blob b that are
// strictly smaller than p.  Works even if p is out of range.
uint64_t eval( const_blob b, uint64_t p ) ;


// Returns a pointer to the beginning of the L vector.  It's stored
// directly behind the header word, and it contains exactly npos items.
static inline uint64_t const *vecL( const_blob b ) { return b+2 ; }

// Returns a pointer to the beginning of the U vector.  It's stored
// behind L, but L is padded to a full qword.
static inline uint64_t const *vecH( const_blob b ) { return vecL(b) + ((npos(b) * paramL(b) + 63) >> 6) ; }
static inline uint64_t       *vecH_mut(   blob b ) { return  b + 2  + ((npos(b) * paramL(b) + 63) >> 6) ; }

// Returns a pointer to the inventory for select operations.  It follows
// the H vector, which is padded to a full qword.
static inline uint64_t const *inventory( const_blob b ) { return vecH(b)     + ((npos(b) + (maxpos(b) >> paramL(b)) + 63) >> 6) ; }
static inline uint64_t         *inventory_mut( blob b ) { return vecH_mut(b) + ((npos(b) + (maxpos(b) >> paramL(b)) + 63) >> 6) ; }

static inline uint32_t get_inv( const_blob b, uint64_t x ) { return inventory(b)[ x >> 1 ] >> (32 * (x & 1)) ; }

static inline void set_inv( blob b, uint64_t x, uint32_t y )
{
    if( x & 1 ) {
        inventory_mut(b)[ x >> 1 ] &= 0xffffffffull ;
        inventory_mut(b)[ x >> 1 ] |= (uint64_t)y << 32ull ;
    }
    else {
        inventory_mut(b)[ x >> 1 ] &= 0xffffffff00000000ull ;
        inventory_mut(b)[ x >> 1 ] |= y ;
    }
}

// Returns a pointer just past the end of the blob.
static inline uint64_t const *blobend( const_blob b ) { return inventory(b) + (((maxpos(b) >> (paramL(b) + 8)) +1) >> 1) ; }

// Entries in hash table.  Maps sequence names to reference indices;
// collisions spill into a linked list.
typedef struct hentry {
    char *key ;
    int   value ;
    struct hentry *next ;
} hentry_t ;

// An index file in memory.  Layout of the index file:
//
// - 8 magic bytes
// - file offset of the phi blob in qword units (qword)
// - file offset of the psi blob in qword units (qword)
// - number of reference sequences NREF (dword)
// - four unused bytes
// - for each reference sequence
//   - end offset in linear genome (qword)
//     (note: the end offset of the last reference sequence is the genome length)
// - hashtable of pointers to sequence names (2*NREF dwords)
// - for each reference sequence
//   - sequence number (dword)
//   - NUL-terminated sequence name
//   - padding to a full dword
// - padding to a full qword
// - two EF blobs (psi & phi)
//   - total number of indexed reads (qword)
//   - largest coordinate (qword)
//   - L vector
//   - H vector
//   - inventory
//
// The struct contains the base pointer and the file length, which are
// mmapped en-bloc, and a hash table for mapping misspelled sequence
// names.

typedef struct index {
    uint64_t *base ;
    ssize_t len ;
    char *fname ;
    hentry_t *nmmap ;
} index_t ;

index_t open_index_file( char *ifile, int nommap ) ;
void close_index_file( index_t* ) ;

static inline blob     get_phi( index_t *ix )                 { return ix->base + ix->base[1] ; }
static inline blob     get_psi( index_t *ix )                 { return ix->base + ix->base[2] ; }

static inline uint32_t get_nref( index_t *ix )                { return ix->base[3] & 0xffffffff ; }
static inline uint64_t get_start_offset( index_t *ix, int k ) { return k ? ix->base[3 + k] : 0 ; }
static inline uint64_t get_end_offset( index_t *ix, int k )   { return ix->base[4 + k] ; }
static inline uint64_t get_genome_len( index_t *ix )          { return ix->base[ 3 + get_nref(ix) ] ; }

// Computes the size of a blob before constructing it.
size_t blobsize2( uint64_t num_ent, uint64_t max_ent ) ;

void   query_fd( index_t*, index_t*, int,   char*, int ) ;
void query_file( index_t*, index_t*, char*, char*, int ) ;
void query_line( index_t*, index_t*, char*, char*, int ) ;

struct ef_builder {
    uint64_t *memchunk ;
    size_t num_mem ;
    size_t cap_mem ;
    blob blobs[64] ;
} ;

struct index_builder {
    char **seq_names ;          // RNAMEs from bam
    uint64_t *offsets ;         // end(!) offsets in linearized genome
    int nref ;                  // number of reference sequences
    uint64_t total_size ;
    blob phi ;
    blob psi ;
} ;

// Reads a Bam file from file destriptor 'fd' into a 'struct
// index_builder'.  'fname' is the file name used in error messages.  If
// 'issorted' is true, it returns early upon encountering the first read
// with an invalid rname.
struct index_builder readBam( int fd, const char* fname, int issorted ) ;

// Parallelized version.
struct index_builder pReadBam( int fd, const char *fname, int issorted, int nthreads ) ;

blob blob_from_sorted_array( uint64_t *arr, size_t num_ent ) ;
blob blob_from_array( uint64_t *arr, size_t num_ent ) ;
blob blob_from_blobs( blob u, blob v ) ;
blob collapse_blobs( blob blobs[64], blob the_blob ) ;
void push_ef_at_ef( blob blobs[64], blob the_blob ) ;

void build_index_file( char *ofile, struct index_builder *seqs ) ;

void init_index_builder( struct index_builder *b, int nref ) ;
void free_index_builder( struct index_builder *b ) ;

void croak(int status, int errnum, const char *format, ...) ;

static inline void add_refseq( struct index_builder *b, char *name, uint64_t len )
{
    b->seq_names[b->nref] = name ;
    b->total_size += len ;
    b->offsets[b->nref] = b->total_size ;
    b->nref++ ;
}

static inline uint64_t eff_pos( struct index_builder *b, int refid, uint64_t x, int rev )
{
    // linearizing the genome:  concatenate the forward strands of every
    // sequence, then append the concatenated reverse strands.
    return ( refid ? b->offsets[refid-1] : 0 ) + (rev ? b->total_size : 0) + x ;
}

static inline uint64_t eff_pos2( struct index_builder *b, int refid, uint64_t x, int rev )
{
    // clamp effective position to reference sequence
    // (no thanks to bwa & co. for producing alignments that hang off
    // the sequence end)
    uint64_t p1 = eff_pos( b, refid, x, rev ) ;
    uint64_t p2 = eff_pos( b, refid+1, 0, rev ) - 1 ;
    return p1 > p2 ? p2 : p1 ;
}

static inline uint64_t bit( const uint64_t *v, uint64_t i ) { return v[ i >> 6ULL ] & (1ULL << (i & 0x3fULL)) ; }

static inline uint64_t bits( const uint64_t *v, uint64_t i, uint64_t n )
{
    uint64_t r = v[ i >> 6ULL ] >> (i & 0x3fULL) ;
    uint64_t nmask = (1ULL << n) -1ULL ;
    if( (i & 0x3fULL) + n > 64ULL ) {
        uint64_t hi = v[ (i >> 6ULL) +1ULL ] ;
        r |= hi << (64ULL - (i & 0x3fULL)) ;
    }
    return r & nmask ;
}

static inline uint32_t hash( uint8_t *str, size_t len )
{
    unsigned long hash = 5381;
    while( len-- )
        hash = ((hash << 5) + hash) + *str++ ;
    return hash;
}

#endif
