/*
    This file is part of Bathometer.

    Bathometer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bathometer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Bathometer.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bathometer.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <zlib.h>

static inline int gzreadint( gzFile f )
{
    int r = gzgetc(f) ;
    r += gzgetc(f) << 8 ;
    r += gzgetc(f) << 16 ;
    r += gzgetc(f) << 24 ;
    return r ;
}

static inline int gzreadshort( gzFile f )
{
    int r = gzgetc(f) ;
    r += gzgetc(f) << 8 ;
    return r ;
}

static inline void init_ef_builder( struct ef_builder *b )
{
    b->num_mem = 0 ;
    b->cap_mem = 1024*1024 ;
    b->memchunk = malloc( b->cap_mem * sizeof(uint64_t) ) ;
    bzero( b->blobs, 64*sizeof( blob ) ) ;
}

static inline void free_ef_builder( struct ef_builder *b )
{
    free( b->memchunk ) ;
    for( int i = 0 ; i != 64 ; ++i ) free( b->blobs[i] ) ;
}

void init_index_builder( struct index_builder *b, int nref )
{
    b->seq_names = calloc( nref, sizeof( char* ) ) ;
    b->offsets = calloc( nref, sizeof( uint64_t ) ) ;
    b->nref = 0 ;
    b->total_size = 0 ;
    b->phi = 0 ;
    b->psi = 0 ;
}

void free_index_builder( struct index_builder *b )
{
    for( int i = 0 ; i != b->nref ; ++i ) free( b->seq_names[i] ) ;
    free( b->seq_names ) ;
    free( b->offsets ) ;
    free( b->phi ) ;
    free( b->psi ) ;
}

struct blob_builder {
    blob blob ;

    size_t bitpos_lo, bitpos_hi ;
    uint64_t bitnum, po ;
    uint64_t acc_lo, acc_hi ;
} ;

static inline void init_blob_builder( struct blob_builder *b, uint64_t num_ent, uint64_t max_ent )
{
    size_t sz = blobsize2( num_ent, max_ent ) ;
    b->blob = malloc( sz * sizeof( uint64_t ) ) ;
    b->blob[0] = num_ent ;
    b->blob[1] = max_ent ;
    b->blob[sz-1] = 0 ;             // set the padding to 0, if there is any (keeps valgrind quiet)

    b->bitpos_lo = 0 ;
    b->bitpos_hi = 0 ;
    b->bitnum = 0 ;
    b->po = 0 ;
    b->acc_lo = 0 ;
    b->acc_hi = 0 ;
}

static inline void push_at_blob( struct blob_builder *b, uint64_t pos )
{
    uint64_t my_l = paramL(b->blob) ;
    uint64_t my_l_mask = (1 << my_l) - 1 ;
    uint64_t *my_L = b->blob +2 ;
    uint64_t *my_H = vecH_mut(b->blob) ;

    // store the l lowest bits of the
    // position into the L vector, LSB first.
    uint64_t p = pos & my_l_mask ;
    b->acc_lo |= p << (b->bitpos_lo & 0x3F) ;
    if( (b->bitpos_lo & 0x3F) + my_l >= 64 )
    {
        my_L[ b->bitpos_lo >> 6 ] = b->acc_lo ;
        b->acc_lo = p >> (64 - (b->bitpos_lo & 0x3F)) ;
    }
    b->bitpos_lo += my_l ;

    // code the difference in the upper bits
    // of the position in unary and store it in the U vector:  every
    // time the upper bits are incremented, append a one.  Append a zero
    // for each stored position.
    //
    // also build the inventory:  count the number of one bits in
    // bitnum.  Every time bitnum is divisible by 256, store the next
    // bitpos in I.
    //
    // if inv[ x ] == u then count1( vec[0,u) ) == 256*(x+1)
    uint64_t pn = pos >> my_l ;
    for( ; b->po != pn ; ++b->po )
    {
        b->acc_hi |= 1ULL << (b->bitpos_hi & 0x3F) ;
        if( !(++b->bitpos_hi & 0x3F) )
        {
            my_H[ (b->bitpos_hi >> 6) - 1 ] = b->acc_hi ;
            b->acc_hi = 0 ;
        }
        if( !(++b->bitnum & 0xFF) )
            set_inv( b->blob, (b->bitnum >> 8) - 1, b->bitpos_hi ) ;
    }
    if( !(++b->bitpos_hi & 0x3F) )
    {
        my_H[ (b->bitpos_hi >> 6) - 1 ] = b->acc_hi ;
        b->acc_hi = 0 ;
    }
}

static inline blob finish_blob( struct blob_builder *b )
{
    uint64_t *my_L = b->blob +2 ;
    uint64_t *my_H = vecH_mut(b->blob) ;

    if( b->bitpos_lo & 0x3F ) my_L[ b->bitpos_lo >> 6 ] = b->acc_lo ;
    if( b->bitpos_hi & 0x3F ) my_H[ b->bitpos_hi >> 6 ] = b->acc_hi ;

    assert( my_L + ((b->bitpos_lo+63) >> 6)            ==  vecH(b->blob) ) ;
    assert( b->bitnum                                  ==  maxpos(b->blob) >> paramL(b->blob) ) ;
    assert( blobend(b->blob) - inventory_mut(b->blob)  ==  (b->bitnum + 256) >> 9 ) ;
    assert( inventory_mut(b->blob) - my_H              ==  (b->bitpos_hi+63) >> 6 ) ;

    return b->blob ;
}

struct blob_iter {
    blob blob ;
    uint64_t i_lo ;
    uint64_t i_hi ;
    uint64_t hibits ;
} ;

static inline uint64_t next_iter( struct blob_iter *i )
{
    if( i->i_lo == npos(i->blob) ) return UINT64_MAX ;
    uint64_t lobits = bits( vecL(i->blob), i->i_lo++ * paramL(i->blob), paramL(i->blob) ) ;
    while( bit( vecH(i->blob), i->i_hi++ ) ) i->hibits++ ;
    return lobits | (i->hibits << paramL(i->blob)) ;
}

static inline uint64_t start_iter( struct blob_iter *i, blob b )
{
    i->blob = b ;
    i->i_lo = 0 ;
    i->i_hi = 0 ;
    i->hibits = 0 ;
    return next_iter( i ) ;
}


blob blob_from_blobs( blob u, blob v )
{
    uint64_t my_maxpos = maxpos(u) > maxpos(v) ? maxpos(u) : maxpos(v) ;
    uint64_t u_L = paramL(u), v_L = paramL(v) ;
    const uint64_t *u_H = vecH(u), *v_H = vecH(v) ;

    blob blob = malloc( blobsize2( npos(u) + npos(v), my_maxpos ) * sizeof( uint64_t ) ) ;
    blob[0] = npos(u) + npos(v) ;
    blob[1] = my_maxpos ;

    size_t bitpos_lo = 0 ;
    size_t bitpos_hi = 0 ;
    uint64_t bitnum = 0 ;
    uint64_t po = 0 ;
    uint64_t acc_lo = 0 ;
    uint64_t acc_hi = 0 ;

    uint64_t my_l = paramL(blob) ;
    uint64_t my_l_mask = (1 << my_l) - 1 ;
    uint64_t *my_L = blob +2 ;
    uint64_t *my_H = vecH_mut(blob) ;

    uint64_t i_lo = 0 ;
    uint64_t i_hi = 0 ;
    uint64_t i_hibits = 0 ;

    uint64_t j_lo = 0 ;
    uint64_t j_hi = 0 ;
    uint64_t j_hibits = 0 ;

    uint64_t a = UINT64_MAX, b = UINT64_MAX ;
    if( i_lo != npos(u) ) {
        uint64_t lobits = bits( vecL(u), i_lo++ * u_L, u_L ) ;
        while( bit( u_H, i_hi++ ) ) i_hibits++ ;
        a = lobits | (i_hibits << u_L) ;
    }

    if( j_lo != npos(v) ) {
        uint64_t lobits = bits( vecL(v), j_lo++ * v_L, v_L ) ;
        while( bit( v_H, j_hi++ ) ) j_hibits++ ;
        b = lobits | (j_hibits << v_L) ;
    }

    while( a != UINT64_MAX || b != UINT64_MAX )
    {
        uint64_t c ;
        if( a < b ) {
            c = a ;
            if( i_lo == npos(u) ) a = UINT64_MAX ;
            else {
                uint64_t lobits = bits( vecL(u), i_lo++ * u_L, u_L ) ;
                while( bit( u_H, i_hi++ ) ) i_hibits++ ;
                a = lobits | (i_hibits << u_L) ;
            }
        } else {
            c = b ;
            if( j_lo == npos(v) ) b = UINT64_MAX ;
            else {
                uint64_t lobits = bits( vecL(v), j_lo++ * v_L, v_L ) ;
                while( bit( v_H, j_hi++ ) ) j_hibits++ ;
                b = lobits | (j_hibits << v_L) ;
            }
        }

        // store the l lowest bits of the
        // position into the L vector, LSB first.
        uint64_t p = c & my_l_mask ;
        acc_lo |= p << (bitpos_lo & 0x3F) ;
        if( (bitpos_lo & 0x3F) + my_l >= 64 )
        {
            my_L[ bitpos_lo >> 6 ] = acc_lo ;
            acc_lo = p >> (64 - (bitpos_lo & 0x3F)) ;
        }
        bitpos_lo += my_l ;

        uint64_t pn = c >> my_l ;
        for( ; po != pn ; ++po )
        {
            acc_hi |= 1ULL << (bitpos_hi & 0x3F) ;
            if( !(++bitpos_hi & 0x3F) )
            {
                my_H[ (bitpos_hi >> 6) - 1 ] = acc_hi ;
                acc_hi = 0 ;
            }
            if( !(++bitnum & 0xFF) )
                set_inv( blob, (bitnum >> 8) - 1, bitpos_hi ) ;
        }
        if( !(++bitpos_hi & 0x3F) )
        {
            my_H[ (bitpos_hi >> 6) - 1 ] = acc_hi ;
            acc_hi = 0 ;
        }
    }

    if( bitpos_lo & 0x3F ) my_L[ bitpos_lo >> 6 ] = acc_lo ;
    if( bitpos_hi & 0x3F ) my_H[ bitpos_hi >> 6 ] = acc_hi ;

    assert( my_L + ((bitpos_lo+63) >> 6)         ==  vecH(blob) ) ;
    assert( bitnum                               ==  my_maxpos >> paramL(blob) ) ;
    assert( blobend(blob) - inventory_mut(blob)  ==  (bitnum + 256) >> 9 ) ;
    assert( inventory_mut(blob) - my_H           ==  (bitpos_hi+63) >> 6 ) ;
    return blob ;
}

static inline int cmpuint64( const void *a, const void *b )
{
    int64_t d = *(int64_t*)a - *(int64_t*)b ;
    return d > 0 ? 1 : d < 0 ? -1 : 0 ;
}

blob blob_from_sorted_array( uint64_t *arr, size_t num_ent )
{
    struct blob_builder b ;
    init_blob_builder( &b, num_ent, arr[num_ent-1] ) ;

    for( uint64_t *x = arr ; x != arr+num_ent ; ++x ) push_at_blob( &b, *x ) ;
    return finish_blob( &b ) ;
}

blob blob_from_array( uint64_t *arr, size_t num_ent )
{
    assert( num_ent > 0 ) ;
    qsort( arr, num_ent, sizeof( uint64_t ), cmpuint64 ) ;
    return blob_from_sorted_array( arr, num_ent ) ;
}

void push_ef_at_ef( blob blobs[64], blob the_blob )
{
    int i = 0 ;
    for( ; blobs[i] ; ++i )
    {
        blob new_blob = blob_from_blobs( the_blob, blobs[i] ) ;
        free( the_blob ) ;
        free( blobs[i] ) ;
        blobs[i] = 0 ;
        the_blob = new_blob ;
    }
    blobs[i] = the_blob ;
}

static inline void push_at_ef( struct ef_builder *b, uint64_t x )
{
    b->memchunk[b->num_mem] = x ;
    b->num_mem++ ;

    if( b->num_mem == b->cap_mem ) {
        push_ef_at_ef( b->blobs, blob_from_array( b->memchunk, b->num_mem ) ) ;
        b->num_mem = 0 ;
    }
}

blob collapse_blobs( blob blobs[64], blob the_blob )
{
    for( int i = 0 ; i != 64 ; ++i )
    {
        if( !the_blob && blobs[i] ) the_blob = blobs[i] ;
        else if( blobs[i] ) {
            blob new_blob = blob_from_blobs( the_blob, blobs[i] ) ;
            free( the_blob ) ;
            free( blobs[i] ) ;
            the_blob = new_blob ;
        }
        blobs[i] = 0 ;
    }
    return the_blob ;
}

static inline blob finish_ef_builder( struct ef_builder *b )
{
    blob the_blob = b->num_mem ? blob_from_array( b->memchunk, b->num_mem ) : 0 ;
    return collapse_blobs( b->blobs, the_blob ) ;
}


/* Reads a BAM file, discards the usual suspects (QC failed, unmapped,
 * vanishing MAPQ), and builds a the Phi and Psi tables.  Returns the
 * index builder containing everything just before the final merge.
 * Memory consumption is about 10% the size of the input BAM file.
 *
 * This function reads many small pieces through the gzFile interface.
 * It looks awfully inefficient, but attempts to optimize this yielded
 * no improved performance.
 */

struct index_builder readBam( int fd, const char *fname, int issorted )
{
    gzFile f ;
    if( fd == -1 || !(f = gzdopen( fd, "rb" )) )
        croak( 1, errno, "could not open %s for reading", fname ) ;
    gzbuffer( f, 0x20000 ) ;

    // read and skip header
    char magic[4] ;
    gzread( f, magic, 4 ) ;
    assert( strncmp( magic, "BAM\1", 4 ) == 0 ) ;

    int hdlen = gzreadint( f ) ;
    z_off_t hdend = gztell(f) + hdlen ;

    // Let's see if the file is sorted after all.  To do this, look for
    // the SO tag in the first line (assumed to be shorter than 80
    // chars) of the header text, then skip over the remaining header.
    // In case of error, assume it isn't sorted and go on without
    // specially handling the error
    if( !issorted ) {
        char hdr_text[80] ;
        issorted = gzgets( f, hdr_text, hdlen >= 80 ? 79 : hdlen ) &&
                   strstr( hdr_text, "\tSO:coordinate" ) ;
    }
    gzseek( f, hdend, SEEK_SET ) ;

    // get number of ref sequences, then their names
    int nref = gzreadint( f ) ;
    struct index_builder builder ;
    init_index_builder( &builder, nref ) ;

    struct ef_builder builder_phi ;
    struct ef_builder builder_psi ;
    init_ef_builder( &builder_phi ) ;
    init_ef_builder( &builder_psi ) ;

    for( int i = 0 ; i != nref ; ++i )
    {
        int lname = gzreadint( f ) ;
        char *name = malloc( lname+1 ) ;
        gzread( f, name, lname ) ;   // supposedly NUL-terminated
        name[lname] = 0 ;            // but I'm not taking any chances
        uint64_t len = gzreadint( f ) ;
        add_refseq( &builder, name, len ) ;
    }

    // read records and store values
    for( int nrec = 0 ;; ++nrec )
    {
        int bsize = gzreadint( f ) ;
        if( gzeof(f) ) break ;
        z_off_t bstart = gztell( f ) ;
        int refid = gzreadint( f ) ;
        int pos   = gzreadint( f ) ;
        int l_read_name = gzgetc( f ) ;
        int mapq = gzgetc( f ) ;
        gzgetc(f) ;
        gzgetc(f) ;          // skip bin
        int n_cigar_op = gzreadshort( f ) ;
        int flag = gzreadshort( f ) ;

        if( issorted && refid == -1 ) break ;

        if( !(nrec & 0xfffff) ) {
            if( 0 <= refid && refid < nref )
                fprintf( stderr, "\033[K%d records, at %s:%d\r", nrec, builder.seq_names[refid], pos ) ;
            else
                fprintf( stderr, "\033[K%d records\r", nrec ) ;
        }

        // ignore if mapq vanishes or the alignment is flagged as any of
        // unmapped, secondary, filtered, duplicate, or supplementary;
        // or if the reference is invalid
        if( mapq && !(flag & 0xf04) && 0 <= refid && refid < nref )
        {
            gzseek( f, 16 + l_read_name, SEEK_CUR ) ;   // l_seq, mrname, mpos, tlen, qname
            // cigar string is next
            int endpos = pos ;
            for( int i = 0 ; i != n_cigar_op ; ++i )
            {
                int cop = gzreadint( f ) ;
                if( (1 << (cop & 0xf)) & 0x18d ) // one of "MDNX="
                    endpos += cop >> 4 ;
            }

            // also ignore empty reads
            if( endpos != pos )
            {
                int reversed = (flag >> 4 ^ flag >> 7) & 1 ;
                push_at_ef( &builder_phi, eff_pos2( &builder, refid, pos, reversed ) ) ;
                push_at_ef( &builder_psi, eff_pos2( &builder, refid, endpos, reversed ) ) ;
            }
        }
        gzseek( f, bstart + bsize, SEEK_SET ) ;
    }
    if( gzclose_r(f)  != Z_OK ) {
        int err ;
        croak( 1, errno, "while decoding %s: %s", fname, gzerror( f, &err ) ) ;
    }
    putc('\n', stderr) ;
    builder.phi = finish_ef_builder( &builder_phi ) ;
    builder.psi = finish_ef_builder( &builder_psi ) ;
    free_ef_builder( &builder_phi ) ;
    free_ef_builder( &builder_psi ) ;
    return builder ;
}

static inline void write_loop( int fd, void* p, size_t l )
{
    while( l ) {
        size_t ll = write( fd, p, l ) ;
        if( ll < 0 ) croak( 1, errno, "trying to write to index" ) ;
        p = (void*)((char*)p + ll) ;
        l -= ll ;
    }
}

void build_index_file( char *ofile, struct index_builder *builder )
{
    blob phi = builder->phi ;
    blob psi = builder->psi ;

    size_t symtab_size = 0 ;                        // in qwords
    for( int i = 0 ; i != builder->nref ; ++i )
        symtab_size += (strlen( builder->seq_names[i] )+8) >> 2 ;
    symtab_size = (symtab_size+1) >> 1 ;

    size_t total_size = 8 * (4 + builder->nref * 2 + blobend(phi) - phi + blobend(psi) - psi + symtab_size) ; // in octets

    int fd = open( ofile, O_CREAT | O_RDWR, 0666 ) ;
    if( !fd ) croak( 1, errno, "trying to create %s", ofile ) ;

    uint64_t header[] =
        { 0x01004D4F48544142                                                    // "BATHOM\0\1"
        , 2 * builder->nref + 4 + symtab_size                                   // offset of phi
        , 2 * builder->nref + 4 + symtab_size + blobend(phi) - phi              // offset of psi
        , builder->nref } ;

    write_loop( fd, header, sizeof( header ) ) ;
    write_loop( fd, builder->offsets, 8 * builder->nref ) ;

    uint32_t *htab = calloc( 8, builder->nref ) ;
    uint32_t *symtab = malloc( 8 * symtab_size ) ;
    symtab[2*symtab_size-2] = symtab[2*symtab_size-1] = 0 ;             // set the padding to 0, if there is any (keeps valgrind quiet)

    uint32_t *psymtab = symtab ;
    for( int i = 0 ; i != builder->nref ; ++i )
    {
        char *nm = (char*)builder->seq_names[i] ;
        int hidx = hash( (uint8_t*)nm, strlen(nm) ) % (2*builder->nref) ;
        while( htab[hidx] ) {
            hidx++ ;
            if( hidx == 2 * builder->nref ) hidx = 0 ;
        }
        htab[hidx] = psymtab - symtab + 4 * builder->nref + 8 ;

        *psymtab++ = i ;
        size_t nmlen = strlen( builder->seq_names[i] ) ;
        memcpy( psymtab, builder->seq_names[i], nmlen ) ;
        size_t padded = (nmlen + 4) & ~3 ;
        memset( (char*)psymtab + nmlen, 0, padded - nmlen ) ;
        psymtab += padded >> 2 ;
    }

    write_loop( fd, htab, 8 * builder->nref ) ;
    write_loop( fd, symtab, 8 * symtab_size ) ;

    write_loop( fd, phi, 8 * (blobend(phi) - phi) ) ;
    write_loop( fd, psi, 8 * (blobend(psi) - psi) ) ;

    free( symtab ) ;
    free( htab ) ;

    assert( total_size == lseek( fd, 0, SEEK_CUR ) ) ;
    if( fsync( fd ) || close( fd ) ) {
        croak( 0, errno, "while flushing index file %s", ofile ) ;
        unlink( ofile ) ;
        exit( 1 ) ;
    }
}

