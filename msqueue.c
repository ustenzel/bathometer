#include "msqueue.h"

#include <assert.h>
#include <sched.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Instead of allocating Nodes on the heap, we have a static array and
// index into it.
// The first node is unused, so that a zero index acts as a null
// pointer.

#define nnodes  4096 
Node nodes[ nnodes ] ;

_Atomic Ptr free_node_list ;

// Links ths nodes into a list, which becomes the free node list.  Must
// be called before doing anything else.
void init_node_list()
{
    for( uint64_t i = 1 ; i != nnodes ; ++i ) 
        atomic_init( &nodes[i].next, i+1 ) ;
    atomic_init( &free_node_list, 1 ) ;
}

static inline uint32_t ptr( Ptr p ) { return p & 0xffffffff ; }
static inline uint32_t cnt( Ptr p ) { return p >> 32 ; }
static inline uint64_t mkptr( uint32_t ptr, uint32_t cnt ) { return (uint64_t)cnt << 32 | ptr ; }

// allocates a node from the free list
uint32_t alloc_node() {
    while(1) {
        Ptr head = free_node_list ;
        if( atomic_compare_exchange_weak(
                    &free_node_list, &head, 
                    mkptr( ptr( nodes[ptr(head)].next ), 1+cnt(head) ) ) ) {
            return ptr(head) ;
        }
    }
}

void free_node( Ptr node )
{
    while(1) {
        Ptr head = free_node_list ;
        nodes[ptr(node)].next = head ;
        if( atomic_compare_exchange_weak(
                    &free_node_list, &head,
                    mkptr( ptr(node), 1+cnt(head) ) ) ) return ;
    }
}
    

void initq( Queue *q ) {
    uint32_t n = alloc_node() ;  //Node *n = malloc( sizeof( Node ) ) ;
    // Ptr p0 = { 0, 0 } ;
    atomic_init( &nodes[n].next, mkptr(0,0) ) ;

    // Ptr pn = { n, 0 } ;
    atomic_init( &q->head, mkptr(n,0) ) ;
    atomic_init( &q->tail, mkptr(n,0) ) ;
}

// assuming the queue is empty, all we need to do is free a single node
void freeq( Queue *q ) {
    assert( ptr(q->head) == ptr(q->tail) ) ;
    free_node( ptr(q->head) ) ;
}

void enqueue( Queue *q, void *data )
{
    uint32_t n = alloc_node() ;
    // Ptr p0 = { 0, 0 } ;
    atomic_init( &nodes[n].next, mkptr(0,0) ) ;
    nodes[n].value = data ;

    while(1) {
        Ptr tail = q->tail ; 
        Ptr next = nodes[ptr(tail)].next ;

        if( !ptr(next) ) {
            // Ptr nodep1 = { n, next.cnt + 1 } ;
            if( atomic_compare_exchange_weak( &nodes[ptr(tail)].next, &next, mkptr(ptr(n),1+cnt(next)) ) ) {
                // Ptr nodep2 = { n, tail.cnt + 1 } ;
                atomic_compare_exchange_weak( &q->tail, &tail, mkptr(ptr(n), 1+cnt(tail)) ) ;
                return ;
            }
        } else {
            // Ptr nextp1 = { next.ptr, tail.cnt + 1 } ;
            atomic_compare_exchange_weak( &q->tail, &tail, mkptr(ptr(next), 1+cnt(tail)) ) ;
        }
    }
}

void *mdequeue( Queue *q )
{
    while(1) {
        Ptr head = q->head ;
        Ptr tail = q->tail ;
        Ptr next = nodes[ptr(head)].next ;

        if( ptr(head) == ptr(tail) ) {
            if( !ptr(next) ) return 0 ;

            // Ptr nextp1 = { next.ptr, tail.cnt+1 } ;
            atomic_compare_exchange_weak( &q->tail, &tail, mkptr( ptr(next), 1+cnt(tail) ) ) ;
        } else {
            void *value = nodes[ptr(next)].value ;
            // Ptr nextp1 = { next.ptr, head.cnt+1 } ;
            if( atomic_compare_exchange_weak( &q->head, &head, mkptr( ptr(next), 1+cnt(head) ) ) ) {
                free_node( ptr(head) ) ;
                return value ;
            }
        }
    }
}

void *dequeue( Queue *q, _Atomic int *c )
{
    int backoff = 0 ;
    while(1) {
        void *x = mdequeue( q ) ;
        if( x ) return x ;

        if( c && atomic_load( c ) ) return 0 ;

        // queue is empty.  allow someone else to run, then sleep for
        // increasingly long intervals
        if( backoff ) {
            usleep( backoff ) ;
            backoff *= 2 ;
            if( backoff > 100000 ) backoff = 100000 ;
        } else {
            sched_yield() ;
            backoff = 100 ;
        }
    }
}
