/* Michael/Scott lock free multi producer, multi consumer queue.
 */

#ifndef INCLUDED_MSQUEUE_H
#define INCLUDED_MSQUEUE_H

#include <stdint.h>

/* Unfortunately, CMPXCHG16B is pretty much unavailable on x86_64, which
 * is also the most important target.  However, CMPXCHG8B *is* available
 * on both i686 and x86_64 using the standard atomic functions, so we
 * use a 32 bit array index and a 32 bit counter packed into a 64 bit
 * integer as a workaround. */

typedef uint64_t Ptr ;

typedef struct _Node {
    _Atomic Ptr next ;
    void *value ;
} Node ;

typedef struct _Queue {
    _Atomic Ptr head ;
    _Atomic Ptr tail ;
} Queue ;

void init_node_list() ;

// inititalized a queue (allocates a dummy node)
void initq( Queue *q ) ;

// free a queues
void freeq( Queue *q ) ;

// enqueues an object
// cannot fail, unless we run out memory
void enqueue( Queue *q, void *data ) ;

// tries to dequeue an object
// if the queue is empty, returns null
void *mdequeue( Queue *q ) ;

// Tries to dequeue from q.  If that fails, checks *c.  If c is null or
// *c is false, it sleeps (for progressivly longer times) and then
// retries.
void *dequeue( Queue *q, _Atomic int *c ) ;


#endif
