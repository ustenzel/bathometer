// Parallel Decompressing BAM Reader.  Structure:
//
// We have one dedicated I/O thread filling buffers, one chunker thread
// identifying BGZF blocks, many unpacker threads, one BAM parser
// collecting position information, and many sorters constructing an
// index.  When the sorters terminate, they merge their indices into
// one.

#include "msqueue.h"
#include "bathometer.h"

#include <assert.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <zlib.h>

const size_t io_chunk_size = 1024*1024 ;
const size_t pos_array_size = 1024*1024 ;

struct bam_reader {
    Queue free_raw_queue ;
    Queue full_raw_queue ;
    Queue empty_bgzf_queue ;
    Queue packed_bgzf_queue ;
    Queue unpacked_bgzf_queue ;
    Queue empty_array_queue ;
    Queue phi_array_queue ;
    Queue psi_array_queue ;

    int fd ;
    int issorted ;
    struct index_builder builder ;
    _Atomic blob psi ;
    _Atomic blob phi ;
    _Atomic int want_exit ;
} ;

// A raw_chunk is a large block for I/O.  It has a reference counter,
// because many small chunks may refer to it, and tracks how much space
// is used.
struct raw_chunk {
    _Atomic int refcount ;
    size_t used ;
    unsigned char *buffer ;
} ;

// The I/O process grabs large raw blocks from free_raw_queue, fill them
// by reading from fd, and sends them along on full_raw_queue.  A final
// empty chunk is sent to signal end-of-stream.
void *io_process( void *args_ )
{
    struct bam_reader *args = (struct bam_reader*)args_ ;
    while(1) {
        struct raw_chunk *chunk = dequeue( &args->free_raw_queue, &args->want_exit ) ;
        if( !chunk ) return 0 ;
        chunk->used = read( args->fd, chunk->buffer, io_chunk_size ) ;
        enqueue( &args->full_raw_queue, chunk ) ;
        if( !chunk->used ) return 0 ;
    }
}

// A bgzf block describes a compressed block in the input stream and an
// output buffer.  It can have up to two input chunks.  A bgzip block
// must refer black to the raw chunks, so those can be reference
// counted.  The seqnum is needed to bring blocks back into order after
// unpacking, 'left' and 'right' are used to form a heap for reordering.

struct bgzf_block {
    struct raw_chunk *chunk1, *chunk2 ;
    unsigned char *buffer ;
    unsigned offset1, offset2 ;
    unsigned size1, size2 ;
    unsigned outsize ;
    unsigned seqnum ;
    struct bgzf_block *left, *right ;
} ;

// The chunker grabs raw chunks from full_raw_queue, and empty bgzip
// block descriptors from empty_bgzf_queue, and sends defined bgzip
// blocks along packed_bgzf_queue.  An empty bgzip block is sent to
// signal end-of-stream *both* to packed_bgzf_queue *and*
// unpacked_bgzf_queue.
void *chunker( void *args_ )
{
    struct bam_reader *args = (struct bam_reader*)args_ ;

    struct raw_chunk *old_raw_chunk = 0 ;
    unsigned current_pos = 0 ;
    unsigned seqnum = 0 ;
    while(1) {
        struct raw_chunk *raw_chunk = dequeue( &args->full_raw_queue, &args->want_exit ) ;
        if( !raw_chunk ) break ;
        atomic_fetch_add( &raw_chunk->refcount, 1 ) ;

        if( old_raw_chunk ) {
            unsigned s1 = old_raw_chunk->used - current_pos ;

            struct bgzf_block *bgzf_block = dequeue( &args->empty_bgzf_queue, &args->want_exit ) ;
            if( !bgzf_block ) break ;
            bgzf_block->chunk1 = old_raw_chunk ;
            bgzf_block->offset1 = current_pos ;
            bgzf_block->size1 = s1 ;

            if( raw_chunk->used ) {
                // figure out the block size *groan*
                unsigned lo = s1 <= 16 ? raw_chunk->buffer[ 16-s1 ] : old_raw_chunk->buffer[ 16+current_pos ] ;
                unsigned hi = s1 <= 17 ? raw_chunk->buffer[ 17-s1 ] : old_raw_chunk->buffer[ 17+current_pos ] ;

                // remaining chunk size becomes new current position
                current_pos = 1 + lo + (hi << 8) - s1 ;

                // fill in the second chunk
                atomic_fetch_add( &raw_chunk->refcount, 1 ) ;
                bgzf_block->chunk2 = raw_chunk ;
                bgzf_block->offset2 = 0 ;
                bgzf_block->size2 = current_pos ;
            }
            old_raw_chunk = 0 ;
            bgzf_block->seqnum = seqnum++ ;
            enqueue( &args->packed_bgzf_queue, bgzf_block ) ;
        }

        if( !raw_chunk->used ) {
            // send the end-of-stream marker
            struct bgzf_block *bgzf_block = dequeue( &args->empty_bgzf_queue, &args->want_exit ) ;
            if( bgzf_block ) {
                bgzf_block->seqnum = seqnum++ ;
                bgzf_block->outsize = ~0 ;
                enqueue( &args->packed_bgzf_queue, bgzf_block ) ;
                enqueue( &args->unpacked_bgzf_queue, bgzf_block ) ;
            }
            // dereference and recycle the unused raw chunk
            atomic_fetch_sub( &raw_chunk->refcount, 1 ) ;
            enqueue( &args->free_raw_queue, raw_chunk ) ;
            return 0 ;
        }

        while(1) {
            // do we have a whole block?
            if( current_pos + 17 >= raw_chunk->used ) break ;

            unsigned lo = raw_chunk->buffer[ current_pos + 16 ] ;
            unsigned hi = raw_chunk->buffer[ current_pos + 17 ] ;
            unsigned bsize = 1 + lo + (hi << 8) ;

            if( raw_chunk->used - current_pos < bsize ) break ;

            struct bgzf_block *bgzf_block = dequeue( &args->empty_bgzf_queue, &args->want_exit ) ;
            if( !bgzf_block ) {
                if( atomic_fetch_sub( &raw_chunk->refcount, 1 ) == 1 ) 
                    enqueue( &args->free_raw_queue, raw_chunk ) ;
                return 0 ;
            }
            atomic_fetch_add( &raw_chunk->refcount, 1 ) ;
            bgzf_block->chunk1 = raw_chunk ;
            bgzf_block->offset1 = current_pos ;
            bgzf_block->size1 = bsize ;
            bgzf_block->chunk2 = 0 ;
            bgzf_block->seqnum = seqnum++ ;
            enqueue( &args->packed_bgzf_queue, bgzf_block ) ;
            current_pos += bsize ;
        }

        if( current_pos == raw_chunk->used ) atomic_fetch_sub( &raw_chunk->refcount, 1 ) ;
        else old_raw_chunk = raw_chunk ;
    }
        
    if( old_raw_chunk && atomic_fetch_sub( &old_raw_chunk->refcount, 1 ) == 1 )
        enqueue( &args->free_raw_queue, old_raw_chunk ) ;
    return 0 ;
}

// Many decompressors grab defined bgzip blocks and inflate single
// blocks at a time.  The bgzip block descriptors are sent along
// packed_bgzf_queue, the raw blocks are dereferenced and recycled back
// to the I/O process once their reference count becomes zero.

void *unpacker( void* args_ )
{
    struct bam_reader *args = (struct bam_reader*)args_ ;
    while(1) {
        struct bgzf_block *bb = dequeue( &args->packed_bgzf_queue, &args->want_exit ) ;
        if( !bb ) return 0 ;
        if( !bb->chunk1 && !bb->chunk2 ) {
            // empty block:  we're done, but we also need to put the
            // block back, so everyone else knows they are done!
            enqueue( &args->packed_bgzf_queue, bb ) ;
            return 0 ;
        }

        assert( bb->chunk1 ) ;
        z_stream ss = { 0 } ;
        ss.next_out = bb->buffer ;
        ss.avail_out = 0x10000 ;
        ss.next_in = bb->chunk1->buffer + bb->offset1 ;
        ss.avail_in = bb->size1 ;
        inflateInit2( &ss, 0x1F ) ;

        if( bb->chunk2 ) {
            int rc = inflate( &ss, Z_NO_FLUSH ) ;
            assert( rc == Z_OK ) ;
            assert( !ss.avail_in ) ;
            ss.next_in = bb->chunk2->buffer + bb->offset2 ;
            ss.avail_in = bb->size2 ;
        }
        int rc = inflate( &ss, Z_FINISH ) ;
        assert( rc == Z_STREAM_END ) ;
        bb->outsize = ss.total_out ;
        inflateEnd( &ss ) ;

        // dereference raw chunks.  if unreferenced, recycle them
        if( bb->chunk1 && atomic_fetch_sub( &bb->chunk1->refcount, 1 ) == 1 )
            enqueue( &args->free_raw_queue, bb->chunk1 ) ;

        if( bb->chunk2 && atomic_fetch_sub( &bb->chunk2->refcount, 1 ) == 1 )
            enqueue( &args->free_raw_queue, bb->chunk2 ) ;

        // pass the bgzf block descriptor.
        bb->chunk1 = bb->chunk2 = 0 ;
        enqueue( &args->unpacked_bgzf_queue, bb ) ;
    }
} ;

// merge two min-seqnum-skew-heaps, returns the merged heap
struct bgzf_block *merge( struct bgzf_block *l, struct bgzf_block *r )
{
    if( !l ) return r ;
    if( !r ) return l ;

    if( l->seqnum < r->seqnum ) {
        struct bgzf_block *m = merge( r, l->right ) ;
        l->right = l->left ;
        l->left = m ;
        return l ;
    } else {
        struct bgzf_block *m = merge( l, r->right ) ;
        r->right = r->left ;
        r->left = m ;
        return r ;
    }
}

// When attempting to read past then end of the stream, cur is set to 0.
// The struct should be initialized to all zeros, then s_fetch_block
// should be called once before trying to extract data.  Attempts to
// extract data after end-of-stream was reached result in disaster.
struct stream {
    struct bgzf_block *heap ;
    struct bgzf_block *cur ;
    Queue *unpacked_bgzf_queue ;
    Queue *empty_bgzf_queue ;
    uint64_t pos ;
    unsigned offset ;
    unsigned next_in_seq ;
} ;

// Fetches the next block in sequence.  Afterwards, s->cur either points
// to a non-empty block, or it is null.  The internal position is
// advanced to the beginnning of the new block.
void s_fetch_block( struct stream *s )
{
    if( s->cur ) s->pos += s->cur->outsize - s->offset ;
    s->offset = 0 ;

    do {
        // recycle old block
        if( s->cur ) enqueue( s->empty_bgzf_queue, s->cur ) ;

        // try to grab the block from the heap
        if( s->heap && s->heap->seqnum == s->next_in_seq ) {
            s->cur = s->heap ;
            s->heap = merge( s->heap->left, s->heap->right ) ;
            s->cur->left = 0 ;
            s->cur->right = 0 ;
        } else {
            // collect blocks from the queue until the correct one arrives
            while(1) {
                s->cur = dequeue( s->unpacked_bgzf_queue, 0 ) ;
                s->cur->left = 0 ;
                s->cur->right = 0 ;
                if( s->cur->seqnum == s->next_in_seq ) break ;
                s->heap = merge( s->heap, s->cur ) ;
            }
        }
        s->next_in_seq++ ;
    } while( s->cur->outsize == 0 ) ;

    if( s->cur->outsize == ~0 ) {
        // we're done  (we simply forget this block, it's a duplicate)
        assert( !s->heap ) ; // srsly, we're done!
        s->cur = 0 ;
    }
}

void free_heap( Queue *q, struct bgzf_block *h )
{
    while( h ) {
        struct bgzf_block *l = h->left, *r = h->right ;
        h->left = h->right = 0 ;
        enqueue( q, h ) ;
        h = l ;
        free_heap( q, r ) ;
    }
}

static inline void s_init( struct stream *s, struct bam_reader *args )
{
    memset( s, 0, sizeof( struct stream ) ) ;
    s->unpacked_bgzf_queue = &args->unpacked_bgzf_queue ;
    s->empty_bgzf_queue = &args->empty_bgzf_queue ;
    s_fetch_block( s ) ;
}

static inline int s_eof( struct stream* s )
{
    return !s->cur ;
}

static inline uint32_t s_getc( struct stream *s )
{
    if( s->offset == s->cur->outsize ) s_fetch_block( s ) ;
    if( !s->cur ) return ~0 ;         // EOS
    s->pos++ ;
    return s->cur->buffer[s->offset++] ;
}

static inline void s_gets( struct stream *s, char *buf, int len )
{
    while( len > s->cur->outsize - s->offset ) {
        int l = s->cur->outsize - s->offset ;
        memcpy( buf, s->cur->buffer + s->offset, l ) ;
        buf += l ;
        len -= l ;
        s_fetch_block( s ) ;
        if( !s->cur ) return ;
    }
    memcpy( buf, s->cur->buffer + s->offset, len ) ;
    s->offset += len ;
    s->pos += len ;
}

void s_seek( struct stream *s, uint64_t p )
{
    uint64_t dist = p - s->pos ;
    while( dist > s->cur->outsize - s->offset ) {
        dist -= s->cur->outsize - s->offset ;
        s_fetch_block( s ) ;
        if( !s->cur ) return ;
    }
    s->offset += dist ;
    s->pos += dist ;
}

uint32_t s_getint_slow( struct stream *s )
{
    uint32_t a = s->cur ? s_getc(s) : 0 ;
    uint32_t b = s->cur ? s_getc(s) : 0 ;
    uint32_t c = s->cur ? s_getc(s) : 0 ;
    uint32_t d = s->cur ? s_getc(s) : 0 ;

    return a << 0  | b << 8  | c << 16 | d << 24 ;
}

static inline uint32_t s_getshort( struct stream *s )
{
    return s_getc( s ) << 0  | s_getc( s ) << 8 ;
}

static inline uint32_t s_getint( struct stream *s )
{
    if( !s->cur ) return ~0 ;
    // happy case: read from buffer
    if( s->offset + 4 < s->cur->outsize ) {
        s->offset += 4 ;
        s->pos += 4 ;
        return (uint32_t)(s->cur->buffer[s->offset-4]) << 0  |
               (uint32_t)(s->cur->buffer[s->offset-3]) << 8  |
               (uint32_t)(s->cur->buffer[s->offset-2]) << 16 |
               (uint32_t)(s->cur->buffer[s->offset-1]) << 24 ;
    }
    return s_getint_slow( s ) ;
}

// The bam parser grabs uncompressed bgzf blocks, reorders them, and
// parses them.  Coordinates are collected in two buffers, buffers are
// passed downstream on two different queues.
void *bam_parser( void* args_ )
{
    struct bam_reader *args = (struct bam_reader*)args_ ;
    struct stream s ;
    s_init( &s, args ) ;

    uint64_t *psibuf = dequeue( &args->empty_array_queue, 0 ) ;
    uint64_t *phibuf = dequeue( &args->empty_array_queue, 0 ) ;
    psibuf[0] = phibuf[0] = 1 ;

    // read and skip header
    uint32_t magic = s_getint( &s ) ;
    assert( !s_eof( &s ) && magic == 0x014D4142 ) ;

    int hdlen = s_getint( &s ) ;
    uint64_t hdend = s.pos + hdlen ;

    // Let's see if the file is sorted after all.  To do this, look for
    // the SO tag in the first line (assumed to be shorter than 80
    // chars) of the header text, then skip over the remaining header.
    // In case of error, assume it isn't sorted and go on without
    // specially handling the error
    if( !args->issorted ) {
        size_t readylen = s.cur->outsize - s.offset ;
        if( hdlen < readylen ) readylen = hdlen ;
        char *eol = memchr( s.cur->buffer + s.offset, '\n', readylen ) ;
        if( eol ) *eol = 0 ;
        else s.cur->buffer[ s.offset + readylen - 1 ] = 0 ;

        args->issorted = !!strstr( (char*)s.cur->buffer + s.offset, "\tSO:coordinate" ) ;
    }
    s_seek( &s, hdend ) ;

    // get number of ref sequences, then their names
    int nref = s_getint( &s ) ;
    init_index_builder( &args->builder, nref ) ;

    for( int i = 0 ; i != nref ; ++i )
    {
        int lname = s_getint( &s ) ;
        char *name = malloc( lname+1 ) ;
        s_gets( &s, name, lname ) ;   // supposedly NUL-terminated
        name[lname] = 0 ;             // but I'm not taking any chances
        uint64_t len = s_getint( &s ) ;
        add_refseq( &args->builder, name, len ) ;
    }

    // read records and store values
    for( int nrec = 0 ;; ++nrec )
    {
        int bsize = s_getint( &s ) ;
        if( s_eof(&s) ) break ;
        uint64_t bstart = s.pos ;
        int refid = s_getint( &s ) ;
        int pos   = s_getint( &s ) ;
        int l_read_name = s_getc( &s ) ;
        int mapq = s_getc( &s ) ;
        s_getc(&s) ;
        s_getc(&s) ;          // skip bin
        int n_cigar_op = s_getshort( &s ) ;
        int flag = s_getshort( &s ) ;

        if( args->issorted && refid == -1 ) break ;

        if( !(nrec & 0xfffff) ) {
            if( 0 <= refid && refid < nref )
                fprintf( stderr, "\033[K%d records, at %s:%d\r", nrec, args->builder.seq_names[refid], pos ) ;
            else
                fprintf( stderr, "\033[K%d records\r", nrec ) ;
        }

        // ignore if mapq vanishes or the alignment is flagged as any of
        // unmapped, secondary, filtered, duplicate, or supplementary;
        // or if the reference is invalid
        if( mapq && !(flag & 0xf04) && 0 <= refid && refid < nref )
        {
            s_seek( &s, bstart + 32 + l_read_name ) ;   // l_seq, mrname, mpos, tlen, qname
            int endpos = pos ;
            for( int i = 0 ; i != n_cigar_op ; ++i )
            {
                int cop = s_getint( &s ) ;
                if( (1 << (cop & 0xf)) & 0x18d ) // one of "MDNX="
                    endpos += cop >> 4 ;
            }

            // also ignore empty reads
            if( endpos != pos )
            {
                int reversed = (flag >> 4 ^ flag >> 7) & 1 ;
                phibuf[ phibuf[0]++ ] = eff_pos2( &args->builder, refid, pos, reversed ) ;
                psibuf[ psibuf[0]++ ] = eff_pos2( &args->builder, refid, endpos, reversed ) ;

                if( phibuf[0] == pos_array_size ) {
                    enqueue( &args->phi_array_queue, phibuf ) ;
                    enqueue( &args->psi_array_queue, psibuf ) ;

                    phibuf = dequeue( &args->empty_array_queue, 0 ) ;
                    psibuf = dequeue( &args->empty_array_queue, 0 ) ;
                    psibuf[0] = phibuf[0] = 1 ;
                }
            }
        }
        s_seek( &s, bstart + bsize ) ;
    }
    putc('\n', stderr) ;
    if( phibuf[0] > 1 ) {
        enqueue( &args->phi_array_queue, phibuf ) ;
        enqueue( &args->psi_array_queue, psibuf ) ;

        psibuf = dequeue( &args->empty_array_queue, 0 ) ;
        phibuf = dequeue( &args->empty_array_queue, 0 ) ;
    }
    psibuf[0] = phibuf[0] = 0 ;
    enqueue( &args->phi_array_queue, phibuf ) ;
    enqueue( &args->psi_array_queue, psibuf ) ;

    atomic_store( &args->want_exit, 1 ) ;
    if( s.cur ) enqueue( s.empty_bgzf_queue, s.cur ) ;
    free_heap( s.empty_bgzf_queue, s.heap ) ;
    return 0 ;
}

static inline int cmpuint64( const void *a, const void *b )
{
    int64_t d = *(int64_t*)a - *(int64_t*)b ;
    return d > 0 ? 1 : d < 0 ? -1 : 0 ;
}

// Array sorter.  If we want more than one of these, the bam parser has
// to send more EOS markers.  (It wouldn't matter if we jumble the order
// of the arrays, but EOS better comes last.)  Each array containd start
// positions in the first half and end positions in the second half.
void *sorter( Queue *empty_array_queue, Queue *full_array_queue, _Atomic blob *out )
{
    blob blobs[64] = {0} ;
    while(1) {
        uint64_t *ibuf = dequeue( full_array_queue, 0 ) ;
        uint64_t sz = *ibuf ;
        if( !ibuf[0] ) {
            // put the end signal back
            enqueue( full_array_queue, ibuf ) ;
            break ;
        }
        push_ef_at_ef( blobs, blob_from_array( ibuf+1, sz-1 ) ) ;
        enqueue( empty_array_queue, ibuf ) ;
    }

    blob the_blob = collapse_blobs( blobs, 0 ) ;
    while(1) {
        blob other_blob = *out ;
        if( other_blob ) {
            // grab that blob and merge it
            if( atomic_compare_exchange_weak( out, &other_blob, 0 ) ) {
                blob new_blob = blob_from_blobs( the_blob, other_blob ) ;
                free( the_blob ) ;
                free( other_blob ) ;
                the_blob = new_blob ;
            }
        } else {
            // empty slot, if we get rid of our blob, we're done
            if( atomic_compare_exchange_weak( out, &other_blob, the_blob ) ) break ;
        }
    }
    return 0 ;
}

void *phi_sorter( void *args_ )
{
    struct bam_reader *args = (struct bam_reader*)args_ ;
    return sorter( &args->empty_array_queue, &args->phi_array_queue, &args->phi ) ;
}

void *psi_sorter( void *args_ )
{
    struct bam_reader *args = (struct bam_reader*)args_ ;
    return sorter( &args->empty_array_queue, &args->psi_array_queue, &args->psi ) ;
}

// This is the main entry point, which serves as a drop-in replacement
// for readBam().  Actual bam parsing is done in bam_parser(), here we
// manage queues and threads and only deal with the file data structure.

struct index_builder pReadBam( int fd, const char *fname, int issorted, int nthreads )
{
    init_node_list() ;
    struct bam_reader args ;
    args.fd = fd ;
    args.issorted = issorted ;
    initq( &args.free_raw_queue ) ;
    initq( &args.full_raw_queue ) ;
    initq( &args.empty_bgzf_queue ) ;
    initq( &args.packed_bgzf_queue ) ;
    initq( &args.unpacked_bgzf_queue ) ;
    initq( &args.empty_array_queue ) ;
    initq( &args.phi_array_queue ) ;
    initq( &args.psi_array_queue ) ;
    atomic_init( &args.phi, 0 ) ;
    atomic_init( &args.psi, 0 ) ;
    atomic_init( &args.want_exit, 0 ) ;

    struct raw_chunk *raw_chunks = malloc( sizeof(struct raw_chunk) * 2 * nthreads ) ;
    for( int i = 0 ; i != 2*nthreads ; ++i ) {
        atomic_init( &raw_chunks[i].refcount, 0 ) ;
        raw_chunks[i].used = 0 ;
        raw_chunks[i].buffer = malloc( io_chunk_size ) ;
        enqueue( &args.free_raw_queue, raw_chunks+i ) ;
    }

    struct bgzf_block *bgzf_blocks = calloc( sizeof(struct bgzf_block), 8*nthreads ) ;
    for( int i = 0 ; i != 8*nthreads ; ++i ) {
        bgzf_blocks[i].buffer = malloc( 0x10000 ) ;
        enqueue( &args.empty_bgzf_queue, bgzf_blocks+i ) ;
    }

    for( int i = 0 ; i != 8 ; ++i ) {
        enqueue( &args.empty_array_queue, malloc( 16 * pos_array_size ) ) ;
    }

    pthread_t io_thread, chunker_thread, *unpackers, parser_thread, *phisorters, *psisorters ;
    pthread_create( &io_thread, 0, io_process, &args ) ;
    pthread_create( &chunker_thread, 0, chunker, &args ) ;
    pthread_create( &parser_thread, 0, bam_parser, &args ) ;
    unpackers  = calloc( nthreads, sizeof( pthread_t ) ) ;
    phisorters = calloc( nthreads, sizeof( pthread_t ) ) ;
    psisorters = calloc( nthreads, sizeof( pthread_t ) ) ;
    for( int i = 0 ; i != nthreads ; ++i ) {
        pthread_create( unpackers+i, 0, unpacker, &args ) ;
        pthread_create( phisorters+i, 0, phi_sorter, &args ) ;
        pthread_create( psisorters+i, 0, psi_sorter, &args ) ;
    }

    pthread_join( parser_thread, 0 ) ;
    for( int i = 0 ; i != nthreads ; ++i ) 
        pthread_join( unpackers[i], 0 ) ;
    free( unpackers ) ;
   
    // If we got out early, the chunker may be stuck on
    // unpacked_bgzf_queue.  This should unstick iot.
    pthread_join( chunker_thread, 0 ) ;
    pthread_join( io_thread, 0 ) ;
    
    for( int i = 0 ; i != nthreads ; ++i ) {
        pthread_join( psisorters[i], 0 ) ;
        pthread_join( phisorters[i], 0 ) ;
    }
    free( phisorters ) ;
    free( psisorters ) ;

    for( uint64_t          *p ; (p = mdequeue( &args.phi_array_queue     )) ; ) free( p ) ;
    for( uint64_t          *p ; (p = mdequeue( &args.psi_array_queue     )) ; ) free( p ) ;
    for( uint64_t          *p ; (p = mdequeue( &args.empty_array_queue   )) ; ) free( p ) ;
    for( struct bgzf_block *p ; (p = mdequeue( &args.empty_bgzf_queue    )) ; ) free( p->buffer ) ;
    for( struct bgzf_block *p ; (p = mdequeue( &args.packed_bgzf_queue   )) ; ) free( p->buffer ) ;
    for( struct bgzf_block *p ; (p = mdequeue( &args.unpacked_bgzf_queue )) ; ) free( p->buffer ) ;
    for( struct  raw_chunk *p ; (p = mdequeue( &args.free_raw_queue      )) ; ) free( p->buffer ) ;
    for( struct  raw_chunk *p ; (p = mdequeue( &args.full_raw_queue      )) ; ) free( p->buffer ) ;

    free( bgzf_blocks ) ;
    free( raw_chunks ) ;

    freeq( &args.psi_array_queue ) ;
    freeq( &args.phi_array_queue ) ;
    freeq( &args.empty_array_queue ) ;
    freeq( &args.unpacked_bgzf_queue ) ;
    freeq( &args.packed_bgzf_queue ) ;
    freeq( &args.empty_bgzf_queue ) ;
    freeq( &args.full_raw_queue ) ;
    freeq( &args.free_raw_queue ) ;

    args.builder.phi = args.phi ;
    args.builder.psi = args.psi ;
    return args.builder ;
}

