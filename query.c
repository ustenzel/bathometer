/*
    This file is part of Bathometer.

    Bathometer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bathometer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Bathometer.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bathometer.h"
#include "select.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <zlib.h>

size_t blobsize2( uint64_t num_ent, uint64_t max_ent )
{
    uint64_t no_blob[2] = { num_ent, max_ent } ;
    return blobend( no_blob ) - no_blob ;
}

index_t open_index_file( char *ifile, int nommap )
{
    index_t ix ;
    int fd = open( ifile, O_RDONLY, 0 ) ;
    if( fd < 0 ) croak( 1, errno, "opening index file %s", ifile ) ;
    off_t len = ix.len = lseek( fd, 0, SEEK_END ) ;

    if( nommap ) {
        ix.base = malloc( len ) ;
        if( !ix.base )
            croak( 1, errno, "while allocation memory for the index" ) ;

        lseek( fd, 0, SEEK_SET ) ;
        for( char* p = (char*)ix.base ; ix.len ; ) {
            ssize_t l = read( fd, p, ix.len ) ;
            if( l < 0 ) croak( 1, errno, "while reading index file" ) ;
            p += l ;
            ix.len -= l ;
        }
    } else {
        ix.base = mmap( 0, len, PROT_READ, MAP_SHARED, fd, 0 ) ;
        if( ix.base == (void*)(-1) )
            croak( 1, errno, "while mmaping index file" ) ;
    }
    close( fd ) ;
    if( len < 8 || memcmp( ix.base, "BATHOM\0\1", 8 ) )
        croak( 1, 0, "%s does not look like an oceanographic map", ifile ) ;

    // should be bloody big enough
    ix.fname = strdup( ifile ) ;
    ix.nmmap = calloc( 2*get_nref(&ix), sizeof( hentry_t ) ) ;
    return ix ;
}

void close_index_file( index_t *ix )
{
    if( ix->nmmap ) {
        for( int i = 0 ; i < 2*get_nref( ix ) ; ++i )
        {
            for( hentry_t *p = ix->nmmap[i].next ; p ; )
            {
                hentry_t *q = p->next ;
                free( p->key ) ;
                free( p ) ;
                p = q ;
            }
            free( ix->nmmap[i].key ) ;
        }
        free( ix->nmmap ) ;
    }
    free( ix->fname ) ;
    if( ix->len ) munmap( ix->base, ix->len ) ;
    else free( ix->base ) ;
}

//! Computes the number values less than p stored in b.  We first
//! compute the number of stored positions whose upper bits are strictly
//! smaller than those of p.  Then we look at each position with
//! matching upper bits and count those that have smaller lower bits.

uint64_t eval( const_blob b, uint64_t p )
{
    if( !b ) return 0 ;
    if( p > maxpos( b ) ) return npos( b ) ;

    uint64_t u0 = 0, n0 = 0 ;
    uint64_t my_l = paramL(b) ;

    // If the upper bits are clear, we start from the beginning.
    // Otherwise we jump ahead by selecting the correct one in vecH.
    if( p >> my_l )
    {
        u0 = select_blob( b, (p >> my_l) -1ULL ) + 1 ;

        // vecH[0,u0) has (p >> my_l) ones
        // vecH[u0-1] is one (vecH is always long enough)
        //
        // vecH[0,u0) has (u0 - (p >> my_l)) zeros
        //
        // n0, the number of zeros in vecH[0,u0) is now the number of
        // positions stored that have upper bits smaller than p
        n0 = u0 - (p >> my_l) ;
    }

    // If u0 starts a stretch of zeros, those are stored positions whose
    // upper bits match those of p, which we need to inspect:  H[u0+i]
    // (i >= 0) is a position smaller than p iff L[n0+i] is less than
    // the low bits of p
    while(1)
    {
        if( bit( vecH(b), u0 ) ) break ;
        uint64_t lbits = bits( vecL(b), n0 * my_l, my_l ) ;
        uint64_t pbits =  (p & ((1ULL << my_l) - 1ULL)) ;
        if( lbits >= pbits ) break ;
        ++n0, ++u0 ;
    }
    return n0 ;
}

void query_gzfile( index_t *ixb, index_t *ixe, gzFile f, char* h, int want_fpkm )
{
    char buf[4096] ;
    while( gzgets( f, buf, 4096 ) )
    {
        char* plf = strchr( buf, '\n' ) ;
        if( plf ) *plf = 0 ;
        query_line( ixb, ixe, buf, h, want_fpkm ) ;
        h = 0 ;
    }
}

void query_fd( index_t *ixb, index_t *ixe, int fd, char* h, int want_fpkm )
{
    gzFile f = gzdopen( fd, "rb" ) ;
    if( !f ) croak( 1, errno, "could not open stream for reading queries" ) ;

    query_gzfile( ixb, ixe, f, h, want_fpkm ) ;
    if( gzclose_r(f) != Z_OK ) {
        int err ;
        croak( 1, errno, "while decoding queries: %s", gzerror( f, &err ) ) ;
    }
}

void query_file( index_t *ixb, index_t *ixe, char* fn, char* h, int want_fpkm )
{
    gzFile f = gzopen( fn, "rb" ) ;
    if( !f ) croak( 1, errno, "could not open %s for reading queries", fn ) ;

    query_gzfile( ixb, ixe, f, h, want_fpkm ) ;
    if( gzclose_r(f) != Z_OK ) {
        int err ;
        croak( 1, errno, "while decoding queries from %s: %s", fn, gzerror( f, &err ) ) ;
    }
}

int contains( char *a, int la, char *b, int lb, int nmiss )
{
    if( la < lb ) {
        char *c = b ; b = a ; a = c ;
        int lc = lb ; lb = la ; la = lc ;
    }

    for( char *x = a ; x != a + la - lb + 1 ; ++x )
    {
        int m = nmiss ;
        for( char *y = b, *z = x ; m >= 0 ; ++y, ++z )
        {
            if( y == b + lb ) return 1 ;
            if( z == a + la ) break ;
            if( *z != *y ) --m ;
        }
    }
    return 0 ;
}


//! Traverses the list of reference sequences, looking for those that
//! contain nm (of length nmlen) or are contained by it, allowing for up
//! to nmiss differences in the overlapping part.  If the matching
//! sequence with minimal length difference is unique, its name is
//! stored in *mname and its index is returned.  If ambiguous matches
//! are found, -1 is returned.  If no match is found, -2 is returned.

int match_refseq( index_t *ix, char *nm, int nmlen, char **mname, int nmiss )
{
    int mind = INT_MAX, midx = -2, nummin = 0 ;

    char *rnm = (char*)( ix->base + 4 + 2 * get_nref( ix ) ) + 4 ;
    for( int i = 0 ; i != get_nref( ix ) ; ++i )
    {
        int rlen = strlen(rnm) ;
        if( contains( nm, nmlen, rnm, rlen, nmiss ) )
        {
            int d = abs( strlen(rnm) - nmlen ) ;
            if( d < mind ) {
                mind = d ;
                nummin = 0 ;
                *mname = rnm ;
                midx = i ;
            }
            else if( d == mind ) {
                nummin++ ;
            }
        }
        rnm += (rlen + 8) & ~3 ;
    }
    return nummin ? -1 : midx ;
}

//! Maps a sequence name to its index.  Returns -1 if the sequence
//! isn't found.  Ideally, this is a simple hash table lookup.
//! Practically, in the world of Bioinformatics, things rarely hve only
//! one name.  Our coping strategy:
//!
//! - If a sequence name is found in the hash table, its index is returned.
//! - Else, if the sequence name is cached, its cached index is returned.
//! - Else, resolve the query string as follows, print an appropriate
//!   warning message, and cache the result:
//!   - Consider all names that contain the query string, or are
//!     contained in the query string.
//!   - If there is a single such name that is closest in length to the
//!     query, map the query to its index.
//!   - If there is no such name, repeat the process, but allow for one
//!     mismatch.
//!   - Else, map the query to negative one.
//!
//! We use a home grown hash table for the cache.

static inline int find_refseq( index_t *ix, char *nm, int nmlen )
{
    // direct lookup in hash table contained in index file
    uint32_t *htab = (uint32_t*)( ix->base + 4 + get_nref(ix) ) ;
    int hashval = hash((uint8_t*)nm, nmlen) % (2*get_nref(ix)) ;
    for( int hidx = hashval ; htab[hidx] ; ) {
        char* rnm = (char*)( (uint32_t*)(ix->base) + htab[hidx] + 1 ) ;
        if( !strncmp( rnm, nm, nmlen ) && strlen( rnm ) == nmlen )
            return *( (uint32_t*)(ix->base) + htab[hidx] ) ;
        hidx++ ;
        if( hidx == 2*get_nref(ix) ) hidx = 0 ;
    }

    // find or create cache entry
    hentry_t *cache_ent = ix->nmmap + hashval ;
    for( ; cache_ent-> key ; cache_ent = cache_ent->next ) {
        if( !strncmp( cache_ent->key, nm, nmlen ) && strlen( cache_ent->key ) == nmlen )
            return cache_ent->value ;
        if( !cache_ent->next )
            cache_ent->next = calloc( 1, sizeof( hentry_t ) ) ;
    }
    cache_ent->key = strndup( nm, nmlen ) ;

    char *mname ;
    int midx = match_refseq( ix, nm, nmlen, &mname, 0 ) ;
    if( midx < -1 ) midx = match_refseq( ix, nm, nmlen, &mname, 1 ) ;
    if( midx < 0 )
        fprintf( stderr, "Warning: %s: target %.*s not found, nor any good replacement.\n", ix->fname, nmlen, nm ) ;
    else
        fprintf( stderr, "Warning: %s: target %.*s not found, but %s will take its place.\n", ix->fname, nmlen, nm, mname ) ;

    cache_ent->value = midx ;
    return midx ;
}

static inline uint64_t query_region( index_t *ix, char *nm, int nmlen, uint64_t start, uint64_t end, char str )
{
    int rname = find_refseq( ix, nm, nmlen ) ;
    if( rname == -1 ) return 0 ;      // sequence not found

    uint64_t eff_start = get_start_offset( ix, rname ) + start + 1 ;
    uint64_t eff_end   = get_start_offset( ix, rname ) + end ;
    uint64_t gg        = get_genome_len( ix ) ;

    if( eff_start >= get_end_offset( ix, rname ) ) eff_start = get_end_offset( ix, rname ) - 1 ;
    if( eff_end   >  get_end_offset( ix, rname ) ) eff_end   = get_end_offset( ix, rname ) ;

    return (str == '+' ? 0 : eval( get_phi( ix ), eff_end + gg ) - eval( get_psi( ix ), eff_start + gg ))
         + (str == '-' ? 0 : eval( get_phi( ix ), eff_end )      - eval( get_psi( ix ), eff_start )) ;
}

void query_regions( index_t *ixb, index_t *ixe, char *nm, int nmlen, uint64_t start, uint64_t end, char str, int want_fpkm )
{
    for( ; ixb != ixe ; ++ixb ) {
        uint64_t count = query_region( ixb, nm, nmlen, start, end, str ) ;
        if( want_fpkm ) {
            // FPKM = fragments per kilobase per million reads mapped
            // Two reads are one fragment. Yeah, it's almost an exact science...
            double fpkm = 5.E8 * count / (end-start) / npos( get_phi( ixb ) ) ;
            printf( "\t%f", fpkm ) ;
        }
        else printf( "\t%"PRIu64, count ) ;
    }
}

void query_line( index_t *ixb, index_t *ixe, char* line, char* h, int want_fpkm )
{
    int nmlen, plen ;
    char c ;
    uint64_t start, end ;

    // If we're supposed to generate a header, check if this is a header and extend it if so.
    // Else make up a BED style header and process the line as normal.
    if( h && (*line == '#' || *line == 1) )
    {
        fputs( line, stdout ) ;
        puts( h ) ;
    }
    else
    {
        if( h )
        {
            fputs( "#chrom\tstart\tend", stdout ) ;
            puts( h ) ;
        }

        if( 3 == sscanf( line, "%*s%n %"SCNu64" %"SCNu64" %*s %*s %c", &nmlen, &start, &end, &c ) )          // BED w/ strand
        {
            fputs( line, stdout ) ;
            query_regions( ixb, ixe, line, nmlen, start, end, c, want_fpkm ) ;
        }
        if( 2 == sscanf( line, "%*s%n %"SCNu64" %"SCNu64"", &nmlen, &start, &end ) )                         // BED
        {
            fputs( line, stdout ) ;
            query_regions( ixb, ixe, line, nmlen, start, end, '*', want_fpkm ) ;
        }
        else if( 3 == sscanf( line, "%*s%n %*s %*s %"SCNu64" %"SCNu64" %*s %c", &nmlen, &start, &end, &c ) ) // GFF w/ strand
        {
            fputs( line, stdout ) ;
            // correction for one-based closed intervals
            query_regions( ixb, ixe, line, nmlen, start-1, end, c, want_fpkm ) ;
        }
        else if( 2 == sscanf( line, "%*s%n %*s %*s %"SCNu64" %"SCNu64, &nmlen, &start, &end ) )              // GFF
        {
            fputs( line, stdout ) ;
            // correction for one-based closed intervals
            query_regions( ixb, ixe, line, nmlen, start-1, end, '*', want_fpkm ) ;
        }
        // cmdline, with and without strand
        else if( 3 == sscanf( line, "%*[^:]%n:%"SCNu64"-%"SCNu64":%c%n", &nmlen, &start, &end, &c, &plen ) && !line[plen] )
        {
            line[plen] = 0 ;
            fputs( line, stdout ) ;
            // correction for one-based closed intervals
            query_regions( ixb, ixe, line, nmlen, start-1, end, c, want_fpkm ) ;
        }
        else if( 2 == sscanf( line, "%*[^:]%n:%"SCNu64"-%"SCNu64"%n", &nmlen, &start, &end, &plen ) && !line[plen] )
        {
            line[plen] = 0 ;
            fputs( line, stdout ) ;
            // correction for one-based closed intervals
            query_regions( ixb, ixe, line, nmlen, start-1, end, '*', want_fpkm ) ;
        }
        else fputs( line, stdout ) ;                                                                // nothing, just echo
        putc( '\n', stdout ) ;
    }
}

