#! /bin/sh

# Giant test.  Will download 800GB of data, eat 2TB of disk space, run
# for days, will create a subdirectory with lots of files.  This script
# makes no effort to parallelize the work on a cluster.  HISAT2 will run
# for quite a while.  You have been warned.

# We try to flush caches in between.  This requires a line for the
# current user in /etc/sudoers looking like this:
#
# elmo ALL=NOPASSWD: /usr/bin/tee /proc/sys/vm/drop_caches

# Everything is done in the current directory, which should reside on a
# sufficiently large disk.  Some measurements should be repeated on an
# SSD, located here:

SSD=/var/tmp/

# We (sometimes) parallelize locally, onto this many cores.

NCORES=32

set -x -e
mkdir -p megatest
cd megatest

# Download Gide et.al. melanoma dataset from ERA.  Too bad the URLs are so irregular.
wget -c ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208887/ipiPD1_1_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208887/ipiPD1_1_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208888/ipiPD1_1_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208888/ipiPD1_1_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208889/ipiPD1_10_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208889/ipiPD1_10_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208890/ipiPD1_12_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208890/ipiPD1_12_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208891/ipiPD1_13_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208891/ipiPD1_13_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208892/ipiPD1_13_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208892/ipiPD1_13_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208893/ipiPD1_14_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208893/ipiPD1_14_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208894/ipiPD1_15_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208894/ipiPD1_15_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208895/ipiPD1_17_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208895/ipiPD1_17_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208896/ipiPD1_19_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208896/ipiPD1_19_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208897/ipiPD1_19_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208897/ipiPD1_19_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208898/ipiPD1_2_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208898/ipiPD1_2_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208899/ipiPD1_20_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208899/ipiPD1_20_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208900/ipiPD1_21_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208900/ipiPD1_21_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208901/ipiPD1_23_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208901/ipiPD1_23_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208902/ipiPD1_24_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208902/ipiPD1_24_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208903/ipiPD1_25_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208903/ipiPD1_25_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208904/ipiPD1_26_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208904/ipiPD1_26_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208905/ipiPD1_27_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208905/ipiPD1_27_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208906/ipiPD1_27_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208906/ipiPD1_27_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208907/ipiPD1_32_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208907/ipiPD1_32_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208908/ipiPD1_33_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208908/ipiPD1_33_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208909/ipiPD1_33_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208909/ipiPD1_33_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208910/ipiPD1_34_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208910/ipiPD1_34_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208911/ipiPD1_35_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208911/ipiPD1_35_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208912/ipiPD1_35_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208912/ipiPD1_35_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208913/ipiPD1_36_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208913/ipiPD1_36_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208914/ipiPD1_36_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208914/ipiPD1_36_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208915/ipiPD1_37_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208915/ipiPD1_37_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208916/ipiPD1_38_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208916/ipiPD1_38_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208918/ipiPD1_44_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208918/ipiPD1_44_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208919/ipiPD1_45_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208919/ipiPD1_45_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208920/ipiPD1_46_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208920/ipiPD1_46_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208921/ipiPD1_47_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208921/ipiPD1_47_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208922/ipiPD1_49_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208922/ipiPD1_49_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208923/ipiPD1_50_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208923/ipiPD1_50_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208924/ipiPD1_6_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208924/ipiPD1_6_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208926/ipiPD1_8_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208926/ipiPD1_8_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208927/ipiPD1_9_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208927/ipiPD1_9_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208928/PD1_10_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208928/PD1_10_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208929/PD1_10_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208929/PD1_10_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208930/PD1_11_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208930/PD1_11_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208931/PD1_12_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208931/PD1_12_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208932/PD1_13_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208932/PD1_13_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208933/PD1_13_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208933/PD1_13_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208934/PD1_14_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208934/PD1_14_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208935/PD1_15_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208935/PD1_15_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208936/PD1_16_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208936/PD1_16_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208937/PD1_17_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208937/PD1_17_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208938/PD1_17_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208938/PD1_17_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208939/PD1_18_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208939/PD1_18_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208940/PD1_19_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208940/PD1_19_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208941/PD1_2_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208941/PD1_2_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208942/PD1_21_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208942/PD1_21_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208943/PD1_23_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208943/PD1_23_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208944/PD1_25_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208944/PD1_25_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208945/PD1_25_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208945/PD1_25_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208946/PD1_27_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208946/PD1_27_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208947/PD1_29_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208947/PD1_29_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208948/PD1_29_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208948/PD1_29_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208949/PD1_3_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208949/PD1_3_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208951/PD1_34_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208951/PD1_34_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208952/PD1_35_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208952/PD1_35_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208953/PD1_36_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208953/PD1_36_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208954/PD1_38_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208954/PD1_38_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208956/PD1_40_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208956/PD1_40_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208957/PD1_41_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208957/PD1_41_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208958/PD1_41_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208958/PD1_41_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208959/PD1_42_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208959/PD1_42_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208960/PD1_44_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208960/PD1_44_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208961/PD1_45_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208961/PD1_45_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208962/PD1_46_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208962/PD1_46_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208963/PD1_47_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208963/PD1_47_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208964/PD1_47_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208964/PD1_47_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208965/PD1_48_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208965/PD1_48_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208966/PD1_48_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208966/PD1_48_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208967/PD1_49_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208967/PD1_49_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208968/PD1_5_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208968/PD1_5_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208969/PD1_50_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208969/PD1_50_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208970/PD1_52_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208970/PD1_52_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208971/PD1_53_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208971/PD1_53_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208972/PD1_54_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208972/PD1_54_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208973/PD1_6_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208973/PD1_6_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208974/PD1_7_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208974/PD1_7_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208975/PD1_8_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208975/PD1_8_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208977/PD1_9_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR220/ERR2208977/PD1_9_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262561/ipiPD1_4_EDT_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262561/ipiPD1_4_EDT_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262562/ipiPD1_7_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262562/ipiPD1_7_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262563/PD1_30_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262563/PD1_30_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262564/PD1_4_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262564/PD1_4_PRE_R2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262565/PD1_8_PRE_R1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/run/ERR326/ERR3262565/PD1_8_PRE_R2.fastq.gz 

# download the example query
wget -c https://bitbucket.org/ustenzel/bathometer/downloads/example_query.bed

# download the reference genome from NCBI
if [[ ! -f GCA_000001405.15_GRCh38_full_analysis_set.fna ]] ; then
    wget -c ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_full_analysis_set.fna.gz

    # prepare the genome for alignment
    gunzip GCA_000001405.15_GRCh38_full_analysis_set.fna.gz
    samtools faidx GCA_000001405.15_GRCh38_full_analysis_set.fna
    hisat2-build GCA_000001405.15_GRCh38_full_analysis_set.fna GCA_000001405.15_GRCh38_full_analysis_set -p $NCORES
fi

# map reads using HISAT2, convert to bam, sort bam

for f in *_R1.fastq.gz ; do
    hisat2 -x GCA_000001405.15_GRCh38_full_analysis_set -1 "${f}" -2 "${f%1.fastq.gz}2.fastq.gz" -p ${NCORES} --dta | \
        samtools view -ut GCA_000001405.15_GRCh38_full_analysis_set.fa.fai - | \
        samtools sort - -o "${f%_R1.fastq.gz}.bam" -T "${SSD}/${f}" 
done 

# invalidate the buffer cache 
sync && echo 3 | sudo tee /proc/sys/vm/drop_caches || true

# index using bathometer
/usr/bin/time /bin/sh -c 'for f in *.bam ; do ../bathometer index "${f%bam}dx" "$f" ; done' \
    2> time_bathometer_index.txt

# copy bathometer indices to ssd, will need later
cp -v *.dx "${SSD}"

# index using samtools.
# (raw data is too big to be in cache at this point)
/usr/bin/time /bin/sh -c 'for f in *.bam ; do samtools index "$f" ; done' \
    2> time_samtools_index.txt

# query using bathometer, from disk
/usr/bin/time ../bathometer query *.dx ::: example_query.bed > example_result_bathometer.txt 2> time_bathometer_query.txt

# do it again, indices are likely cached now
/usr/bin/time ../bathometer query *.dx ::: example_query.bed > example_result_bathometer.txt 2>> time_bathometer_query.txt

# query from SSD; indices aren't needed afterwards
/usr/bin/time ../bathometer query "${SSD}/"*.dx ::: example_query.bed > "${SSD}"/example_result_bathometer.txt 2>> time_bathometer_query.txt
rm "${SSD}/"*.dx 

# copy seven bam files with indices to SSD
cp -v ipiPD1_1_EDT.bam  ipiPD1_1_EDT.bam.bai \
      ipiPD1_1_PRE.bam  ipiPD1_1_PRE.bam.bai \
      ipiPD1_10_PRE.bam ipiPD1_10_PRE.bam.bai \
      ipiPD1_12_PRE.bam ipiPD1_12_PRE.bam.bai \
      ipiPD1_13_EDT.bam ipiPD1_13_EDT.bam.bai \
      ipiPD1_13_PRE.bam ipiPD1_13_PRE.bam.bai \
      ipiPD1_14_PRE.bam ipiPD1_14_PRE.bam.bai \
      "${SSD}"

# invalidate the buffer cache 
sync && echo 3 | sudo tee /proc/sys/vm/drop_caches || true

# query using samtools, 
/usr/bin/time /bin/sh -c 'for f in *.bam ; do samtools view -c -L example_query.bed "$f" ; done' \
        > example_result_samtools.txt 2> time_samtools_query.txt

# do it again, some stuff may be cached now
/usr/bin/time /bin/sh -c 'for f in *.bam ; do samtools view -c -L example_query.bed "$f" ; done' \
        > example_result_samtools.txt 2>> time_samtools_query.txt

# query from SSD; files aren't needed afterwards
/usr/bin/time /bin/sh -c 'for f in '"${SSD}"'*.bam ; do samtools view -c -L example_query.bed "$f" ; done' \
        > "${SSD}"/example_result_samtools.txt 2>> time_samtools_query.txt
rm "${SSD}"/*.bam "${SSD}"/*.bam.bai

cd -

