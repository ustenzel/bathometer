#! /usr/bin/perl

# Quick and dirty testing:  compare bathometer results to those from samtools
# Usage: perl qtest.pl test.bam test.dx

use strict ;
my $f = $ARGV[0] ;
my $fi = $ARGV[1] ;

if( !-f "${f}.bai" && !-f "${f}.csi" ) {
    `samtools index ${f}` ;
}

open HDR, "samtools view -H \"$f\" |" ;
my @refs ;
while(<HDR>) {
    if( /^\@SQ\tSN:([^\t]+)\tLN:([0-9]+)/ ) {
        push @refs, [$1, $2] ;
    }
}
close HDR ;

my $fail = 0 ;
my $pass = 0 ;
for( my $i = 0 ; $i != 1000 ; ++$i ) {
    my $rid = int( rand( scalar @refs ) ) ;

    my $rlen = $refs[$rid]->[1] ;
    my $beg = int(rand($rlen)) + 1 ;
    my $end = int(rand($rlen/2)) + $beg ;
    my $rnm = $refs[$rid]->[0] ;

    print "\e[K$i: ${rnm}:${beg}-${end}\r" ;
    
    # for forward strand
    my $a0 = `../bathometer query "${fi}" ::: "${rnm}:${beg}-${end}:+"` ;
    my $a1 = 0 + `samtools view "$f" -c -q1         -F 0xF94 "${rnm}:${beg}-${end}"` ; #  not second mate, not reversed
    my $a2 = 0 + `samtools view "$f" -c -q1 -f 0x90 -F 0xF04 "${rnm}:${beg}-${end}"` ; #  second mate, reversed
    my ($foo,$a) = split ' ', $a0 ;

    # for reverse strand
    my $b0 = `../bathometer query "${fi}" ::: "${rnm}:${beg}-${end}:-"` ;
    my $b1 = 0 + `samtools view "$f" -c -q1 -f 0x10 -F 0xF84 "${rnm}:${beg}-${end}"` ; # not second mate, reversed
    my $b2 = 0 + `samtools view "$f" -c -q1 -f 0x80 -F 0xF14 "${rnm}:${beg}-${end}"` ; # second mate, not reversed
    my ($bar,$b) = split ' ', $b0 ;

    if( $a1 + $a2 == $a && $b1 + $b2 == $b ) {
        $pass++ ;
    } else {
        print STDERR "Oh shit: ${rnm}:${beg}-${end}   $a1 $a2 $a   $b1 $b2 $b\n" ;
        $fail++ ;
    }
}
print "\e[K${pass} tests passed, ${fail} tests failed.\n"

