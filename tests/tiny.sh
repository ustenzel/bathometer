#! /bin/sh

# Tiny test to demonstrate syntax of bathometer.

set -x -e

# convert supplied sam to bam
[ -f tiny.bam ] || samtools view -b -o tiny.bam tiny.sam

# index using bathometer
[ -f tiny.dx ] || ../bathometer index 0 tiny.dx tiny.bam

# overall summary (there should be 38 reads on I, none on the other chromosomes)
../bathometer summary tiny.dx

# query a few genes by piping GFF in
../bathometer query tiny.dx << EOF
I	WormBase	gene	15060299	15061150	.	+	.	ID=Gene:WBGene00004677
I	WormBase	gene	15060861	15061159	.	-	.	ID=Gene:WBGene00201398
I	WormBase	gene	15062083	15063836	.	+	.	ID=Gene:WBGene00004512
I	WormBase	gene	15063050	15063383	.	-	.	ID=Gene:WBGene00198814
I	WormBase	gene	15064301	15064453	.	+	.	ID=Gene:WBGene00004567
I	WormBase	gene	15064838	15068346	.	+	.	ID=Gene:WBGene00004622
I	WormBase	gene	15067668	15068355	.	-	.	ID=Gene:WBGene00196396
I	WormBase	gene	15069280	15071033	.	+	.	ID=Gene:WBGene00004513
I	WormBase	gene	15070431	15070580	.	-	.	ID=Gene:WBGene00195813
EOF

# same, directly from the command line
../bathometer query tiny.dx ::: \
        I:15060299-15061150:+ \
        I:15060861-15061159:- \
        I:15062083-15063836:+ \
        I:15063050-15063383:- \
        I:15064301-15064453:+ \
        I:15064838-15068346:+ \
        I:15067668-15068355:- \
        I:15069280-15071033:+ \
        I:15070431-15070580:-
