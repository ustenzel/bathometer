#! /bin/sh

# Test on WGS data from a large genome.  We download about 8GB of data
# and an 8GB genome, we'll need around 40GB of disk space.  A directory
# with lots of files will be created.  You have been warned.


# We try to flush caches in between.  This requires a line for the
# current user in /etc/sudoers looking like this:
#
# elmo ALL=NOPASSWD: /usr/bin/tee /proc/sys/vm/drop_caches

# Everything is done in the current directory, which should reside on a
# sufficiently large disk.  Some measurements should be repeated on an
# SSD, located here:

SSD=/var/tmp/

# We (sometimes) parallelize locally, onto this many cores.

NCORES=32

set -x -e
mkdir -p wgstest
cd wgstest

# Download about 8GB random WGS reads of wheat from ERA.  Too bad the URLs are so irregular.
wget -c ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR126/022/SRR12687022/SRR12687022_1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR126/022/SRR12687022/SRR12687022_2.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR126/020/SRR12687020/SRR12687020_1.fastq.gz \
        ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR126/020/SRR12687020/SRR12687020_2.fastq.gz

# download the example query (the annotation for wheat)
if [[ ! -f  iwgsc_refseqv1.0_miRNA_2017Oct27.gff3.zip ]] ; then
    wget https://urgi.versailles.inra.fr/download/iwgsc/IWGSC_RefSeq_Annotations/v1.0/iwgsc_refseqv1.0_miRNA_2017Oct27.gff3.zip
    unzip iwgsc_refseqv1.0_miRNA_2017Oct27.gff3.zip
    ln -s iwgsc_refseqv1.0_miRNA_2017Oct27.gff3 example_query.gff
    # samtools doesn't like gff
    awk '{ print $1, $4 - 1, $5, $3, $6, $7 }' example_query.gff > example_query.bed
fi

# download the reference genome from NCBI
if [[ ! -f Triticum_aestivum.TGACv1.30.dna.genome.fa ]] ; then
    wget -c ftp://ftp.ensemblgenomes.org/pub/plants/pre/fasta/triticum_aestivum/dna/Triticum_aestivum.TGACv1.30.dna.genome.fa.gz

    # prepare the genome for alignment
    gunzip Triticum_aestivum.TGACv1.30.dna.genome.fa.gz
    samtools faidx Triticum_aestivum.TGACv1.30.dna.genome.fa 
    hisat2-build --large-index Triticum_aestivum.TGACv1.30.dna.genome.fa  Triticum_aestivum.TGACv1.30 -p $NCORES
fi

# map reads using HISAT2, convert to bam, sort bam

for f in *_1.fastq.gz ; do
    if [[ ! -f "${f%_1.fastq.gz}.bam" ]] ; then 
        hisat2 -x Triticum_aestivum.TGACv1.30 -1 "${f}" -2 "${f%1.fastq.gz}2.fastq.gz" -p ${NCORES} --dta | \
            samtools view -ut Triticum_aestivum.TGACv1.30.dna.genome.fa.fai - | \
            samtools sort - -o "${f%_1.fastq.gz}.bam" -T "${SSD}/${f}" 
    fi
done 

# invalidate the buffer cache 
sync && echo 3 | sudo tee /proc/sys/vm/drop_caches || true

# index using bathometer
/usr/bin/time /bin/sh -c 'for f in *.bam ; do ../bathometer index "${f%bam}dx" "$f" ; done' \
    2> time_bathometer_index.txt

# copy bathometer indices to ssd, will need later
cp -v *.dx "${SSD}"

# index using samtools.
# (raw data is too big to be in cache at this point)
/usr/bin/time /bin/sh -c 'for f in *.bam ; do samtools index -c "$f" ; done' \
    2> time_samtools_index.txt

# query using bathometer, from disk
/usr/bin/time ../bathometer query *.dx ::: example_query.gff > example_result_bathometer.txt 2> time_bathometer_query.txt

# do it again, indices are likely cached now
/usr/bin/time ../bathometer query *.dx ::: example_query.gff > example_result_bathometer.txt 2>> time_bathometer_query.txt

# query from SSD; indices aren't needed afterwards
/usr/bin/time ../bathometer query "${SSD}/"*.dx ::: example_query.gff > "${SSD}"/example_result_bathometer.txt 2>> time_bathometer_query.txt
rm "${SSD}/"*.dx 

# copy seven bam files with indices to SSD
cp -v *.bam *.bam.csi "${SSD}"

# invalidate the buffer cache 
sync && echo 3 | sudo tee /proc/sys/vm/drop_caches || true

# query using samtools, 
/usr/bin/time /bin/sh -c 'for f in *.bam ; do samtools view -c -L example_query.bed "$f" ; done' \
        > example_result_samtools.txt 2> time_samtools_query.txt

# do it again, some stuff may be cached now
/usr/bin/time /bin/sh -c 'for f in *.bam ; do samtools view -c -L example_query.bed "$f" ; done' \
        > example_result_samtools.txt 2>> time_samtools_query.txt

# query from SSD; files aren't needed afterwards
/usr/bin/time /bin/sh -c 'for f in '"${SSD}"'*.bam ; do samtools view -c -L example_query.bed "$f" ; done' \
        > "${SSD}"/example_result_samtools.txt 2>> time_samtools_query.txt
rm "${SSD}"/*.bam "${SSD}"/*.bam.csi

cd -


